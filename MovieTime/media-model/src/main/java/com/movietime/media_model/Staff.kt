package com.movietime.media_model

interface Staff {
    val profileImage: String?
    val name: String?
    val jobCharacter: String?
}