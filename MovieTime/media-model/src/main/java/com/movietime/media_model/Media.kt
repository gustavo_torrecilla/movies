package com.movietime.media_model

import androidx.annotation.NonNull
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

abstract class Media {
    abstract val name: String
    abstract val isMovie: Boolean
    abstract val isTv: Boolean

    @PrimaryKey
    @NonNull
    @SerializedName("id")
    lateinit var id: String

    @SerializedName("poster_path")
    var posterPath: String? = null

    @SerializedName("backdrop_path")
    var backdropPath: String? = null

    @SerializedName("vote_average")
    lateinit var rating: String

    @SerializedName("overview")
    lateinit var overview: String

    @Ignore
    @SerializedName("genre_ids")
    var genreIds: List<Int> = emptyList()

    constructor() {}
    constructor(
        id: String,
        posterPath: String,
        backdropPath: String,
        rating: String,
        overview: String,
        genreIds: List<Int>
    ) {
        this.id = id
        this.posterPath = posterPath
        this.backdropPath = backdropPath
        this.rating = rating
        this.overview = overview
        this.genreIds = genreIds
    }

    fun getPosterUrl(): String { //todo sacar esto de aca
        return "http://image.tmdb.org/t/p/w500$posterPath"
    }

    fun getBackdropUrl(): String {
        return "http://image.tmdb.org/t/p/original$backdropPath"
    }

}