package com.movietime.media_model

import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity
class Movie : Media {
    @SerializedName("title")
    override var name: String = ""

    //for parcel
    constructor() {}
    constructor(
        id: String?,
        posterUrl: String?,
        backdropUrl: String?,
        rating: String?,
        overview: String?,
        genreIds: List<Int>,
        name: String
    ) : super(id!!, posterUrl!!, backdropUrl!!, rating!!, overview!!, genreIds) {
        this.name = name
    }

    override val isMovie: Boolean
        get() = true

    override val isTv: Boolean
        get() = false


}