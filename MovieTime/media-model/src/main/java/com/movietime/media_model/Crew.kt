package com.movietime.media_model

import com.google.gson.annotations.SerializedName

class Crew : Staff {
    @SerializedName("credit_id")
    var creditId: String? = null

    @SerializedName("department")
    var department: String? = null

    @SerializedName("gender")
    var gender: Int? = null

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("job")
    override var jobCharacter: String? = null
        get() = field
        set

    @SerializedName("name")
    override var name: String? = null

    @SerializedName("profile_path")
    private var profilePath: String? = null

    override val profileImage: String
        get() = "http://image.tmdb.org/t/p/w500$profilePath"

    fun setProfilePath(profilePath: String?) {
        this.profilePath = profilePath
    }
}