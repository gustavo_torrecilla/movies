package com.movietime.media_model

import com.google.gson.annotations.SerializedName

class Cast : Staff {
    @SerializedName("character")
    override var jobCharacter: String? = null
        get() = field
        set

    @SerializedName("credit_id")
    var creditId: String? = null

    @SerializedName("gender")
    var gender: Int? = null

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    override var name: String? = null

    @SerializedName("order")
    var order: Int? = null

    @SerializedName("profile_path")
    private var profilePath: String? = null

    override val profileImage: String
        get() = "http://image.tmdb.org/t/p/w500$profilePath"

    fun setProfilePath(profilePath: String?) {
        this.profilePath = profilePath
    }
}