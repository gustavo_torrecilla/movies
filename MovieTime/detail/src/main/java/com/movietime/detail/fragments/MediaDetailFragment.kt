package com.movietime.detail.fragments

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.provider.Contacts
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.TransitionInflater
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.movietime.base_ui.HorizontalSpaceItemDecoration
import com.movietime.base_ui.flowText.FlowTextHelper
import com.movietime.detail.R
import com.movietime.detail.adapters.MediaAdapter
import com.movietime.detail.adapters.StaffAdapter
import com.movietime.detail.databinding.MediaDetailBinding
import com.movietime.detail.viewmodels.MediaDetailViewModel
import com.movietime.media_model.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MediaDetailFragment: Fragment() {
    private val mediaDetailViewModel: MediaDetailViewModel by viewModels()
    private lateinit var binding: MediaDetailBinding
    private val args: MediaDetailFragmentArgs by navArgs()

    companion object {
        const val FRAGMENT_TYPE_KEY = "fragment_type_key"
        const val TV_TYPE = "tv"
        const val MOVIE_TYPE = "movie"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MediaDetailBinding.inflate(inflater, container, false)
        initializeRecyclerView()
        initializeToolbar()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeData()
    }

    private fun initializeToolbar() {
        (activity as AppCompatActivity).setSupportActionBar(binding.toolbar)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowHomeEnabled(true)
        binding.toolbar.setupWithNavController(findNavController())
    }

    private fun observeData(){
        mediaDetailViewModel.media.observe(viewLifecycleOwner, Observer { media ->
            binding.collapsingToolbar.title = media.name
            initializeTransitionViews(media)
            initializeOverview()
        })
        mediaDetailViewModel.details.observe(viewLifecycleOwner, Observer { mediaDetail ->
//            todo show tagline when separete fragments
//            mediaDetail.tagline?.let { tagline -> if (tagline.isNotEmpty()) showTagLine(tagline) }
            mediaDetail?.genres?.let { genres -> if (genres.isNotEmpty()) showGenres(genres) }
        })

        mediaDetailViewModel.recommendations.observe(viewLifecycleOwner, Observer { recommendations ->
            binding.recommendations.adapter = MediaAdapter(recommendations, context, { media, transitionViews ->})
            appearAnimation(binding.recommendationsTitle)
        })

        mediaDetailViewModel.credits.observe(viewLifecycleOwner, Observer { credits ->
            credits.cast?.let { cast ->
                showCast(cast)
            }

            credits.crew?.let { crew ->
                showCrew(crew)
            }

        })
    }

    fun showTagLine(tagline: String) {
        binding.tagline.text = tagline
        appearAnimation(binding.tagline)
    }

    fun showGenres(genres: List<Genre>) {
        appearAnimation(binding.genreContainer)
        for (genre in genres) {
            val tvGenre = LayoutInflater.from(context)
                .inflate(R.layout.genre_text, binding.genreContainer, false) as TextView
            tvGenre.text = genre.name
            binding.genreContainer.addView(tvGenre)
        }
    }

    private fun initializeRecyclerView() {
        val space = Math.round(
            TypedValue
            .applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5f, resources.displayMetrics))
        val castLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.cast.addItemDecoration(HorizontalSpaceItemDecoration(space))
        binding.cast.layoutManager = castLayoutManager
        val crewLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.crew.addItemDecoration(HorizontalSpaceItemDecoration(space))
        binding.crew.layoutManager = crewLayoutManager
        val trailerLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.trailerList.addItemDecoration(HorizontalSpaceItemDecoration(space))
        binding.trailerList.layoutManager = trailerLayoutManager
        val recommendationsLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.recommendations.addItemDecoration(HorizontalSpaceItemDecoration(space))
        binding.recommendations.layoutManager = recommendationsLayoutManager
    }

    private fun showCrew(crew: List<Crew>){
        binding.crew.adapter = StaffAdapter(crew, context)
        appearAnimation(binding.crewTitle)
        appearAnimation(binding.crew)
    }

    private fun showCast(cast: List<Cast>){
        binding.cast.adapter = StaffAdapter(cast, context)
        appearAnimation(binding.castTitle)
        appearAnimation(binding.cast)
    }

    private fun appearAnimation(view: View) {
        view.visibility = View.VISIBLE
        view.alpha = 0f
        view.animate().alpha(1f).start()
    }

    private fun initializeTransitionViews(media: Media) { //when transition begin I try to load image from cache
        loadPosterImage(media, true)
        binding.rate.text = media.rating
        Glide.with(this)
            .load(media.getBackdropUrl())
            .into(binding.backdropImage)
    }

    private fun loadPosterImage(media: Media, onlyRetrieveFromCache: Boolean) {
        val startTransitionAfterLoadImageListener: RequestListener<Drawable> = object :
            RequestListener<Drawable> {
            override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                return false
            }

            override fun onResourceReady(resource: Drawable, model: Any, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                return false
            }
        }
        Glide.with(this)
            .load(media.getPosterUrl())
            .listener(startTransitionAfterLoadImageListener)
            .apply(
                RequestOptions().dontTransform()
                .onlyRetrieveFromCache(onlyRetrieveFromCache))
            .into(binding.posterImage)
    }

    private fun initializeOverview() {
        viewLifecycleOwner.lifecycleScope.launch {
            delay(500)
            initializeWrappingText()
            appearAnimation(binding.overview)
            appearAnimation(binding.overviewTitle)
        }
    }


    private fun initializeWrappingText() {
        mediaDetailViewModel.media.observe(viewLifecycleOwner, Observer { media ->
            val titleLayoutParams = binding.overviewTitle.layoutParams as ConstraintLayout.LayoutParams
            val overviewLayoutParams = binding.overview.layoutParams as ConstraintLayout.LayoutParams
            val offset = (titleLayoutParams.topMargin + binding.overviewTitle.height
                + overviewLayoutParams.topMargin)
            val display = activity?.windowManager?.defaultDisplay
            FlowTextHelper.flowText(media.overview, binding.posterImage, binding.overview, display, offset)
        })
    }
}