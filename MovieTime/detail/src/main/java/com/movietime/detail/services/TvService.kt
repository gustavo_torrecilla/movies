package com.movietime.detail.services

import com.movietime.media_model.Credits
import com.movietime.media_model.Tv
import com.movietime.detail.model.TvDetail
import com.movietime.media_model.Video
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface TvService {
    @GET("tv/{tvId}/videos")
    fun tvVideos(@Path("tvId") tvId: String?): Single<List<Video>>

    @GET("tv/{tvId}/credits")
    fun tvCredits(@Path("tvId") tvId: String?): Single<Credits>

    @GET("tv/{tvId}/recommendations")
    fun tvRecommendations(@Path("tvId") tvId: String?): Single<List<Tv>>

    @GET("tv/{tvId}")
    fun tvDetails(@Path("tvId") tvId: String?): Single<TvDetail>
}