package com.movietime.detail.repositories

import com.movietime.database.dao.TvDao
import com.movietime.detail.model.MediaDetail
import com.movietime.detail.services.TvService
import com.movietime.media_model.Credits
import com.movietime.media_model.Tv
import com.movietime.detail.model.TvDetail
import com.movietime.media_model.Media
import com.movietime.media_model.Video
import io.reactivex.Single
import javax.inject.Inject

class TvRepository  @Inject constructor(
        private val tvService: TvService,
        private val tvDao: TvDao
    ): MediaRepository {

    override fun mediaVideos(movieId: String): Single<List<Video>> {
        return tvService.tvVideos(movieId)
    }

    override fun mediaCredits(movieId: String): Single<Credits> {
        return tvService.tvCredits(movieId)
    }

    override fun mediaRecommendations(movieId: String): Single<List<Media>> {
        return tvService.tvRecommendations(movieId).map { it }
    }

    override fun mediaDetails(movieId: String): Single<MediaDetail> {
        return tvService.tvDetails(movieId).map { it }
    }

    override fun mediaMainData(mediaId: String): Single<Media> {
        return tvDao.getTv(mediaId).map { it }
    }

}