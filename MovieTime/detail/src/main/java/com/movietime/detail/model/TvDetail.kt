package com.movietime.detail.model

import com.google.gson.annotations.SerializedName
import com.movietime.media_model.Genre
import com.movietime.media_model.Network
import com.movietime.media_model.ProductionCompany
import com.movietime.media_model.Season
import com.movietime.media_model.TvEpisode
import com.movietime.media_model.TvShowCreator

class TvDetail: MediaDetail {
    @SerializedName("backdrop_path")
    override val backdropPath: String? = null

    @SerializedName("created_by")
    val createdBy: List<TvShowCreator>? = null

    @SerializedName("episode_run_time")
    val episodeRunTime: List<Int>? = null

    @SerializedName("first_air_date")
    val firstAirDate: String? = null

    @SerializedName("genres")
    override val genres: List<Genre>? = null

    @SerializedName("homepage")
    override val homepage: String? = null

    @SerializedName("id")
    override val id: Int? = null

    @SerializedName("in_production")
    val inProduction: Boolean? = null

    @SerializedName("languages")
    val languages: List<String>? = null

    @SerializedName("last_air_date")
    val lastAirDate: String? = null

    @SerializedName("last_episode_to_air")
    val lastEpisodeToAir: TvEpisode? = null

    @SerializedName("name")
    val name: String? = null

    @SerializedName("next_episode_to_air")
    val nextEpisodeToAir: Any? = null

    @SerializedName("networks")
    val networks: List<Network>? = null

    @SerializedName("number_of_episodes")
    val numberOfEpisodes: Int? = null

    @SerializedName("number_of_seasons")
    val numberOfSeasons: Int? = null

    @SerializedName("origin_country")
    val originCountry: List<String>? = null

    @SerializedName("original_language")
    val originalLanguage: String? = null

    @SerializedName("original_name")
    val originalName: String? = null

    @SerializedName("overview")
    override val overview: String? = null

    @SerializedName("popularity")
    override val popularity: Double? = null

    @SerializedName("poster_path")
    override val posterPath: String? = null

    @SerializedName("production_companies")
    val productionCompanies: List<ProductionCompany>? = null

    @SerializedName("seasons")
    val seasons: List<Season>? = null

    @SerializedName("status")
    override val status: String? = null

    @SerializedName("type")
    val type: String? = null

    @SerializedName("vote_average")
    override val voteAverage: Double? = null

    @SerializedName("vote_count")
    override val voteCount: Int? = null
}