package com.movietime.detail.viewmodels

import dagger.hilt.android.lifecycle.HiltViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.movietime.base.applyMainSchedulers
import com.movietime.base.plusAssign
import com.movietime.detail.fragments.MediaDetailFragment.Companion.FRAGMENT_TYPE_KEY
import com.movietime.detail.fragments.MediaDetailFragment.Companion.TV_TYPE
import com.movietime.detail.model.MediaDetail
import com.movietime.media_model.Credits
import com.movietime.detail.repositories.MediaRepository
import com.movietime.media_model.Media
import com.movietime.media_model.Video
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject
import javax.inject.Named

@HiltViewModel
class MediaDetailViewModel @Inject constructor(
    @Named("movie_detail_repository") movieRepository: MediaRepository,
    @Named("tv_detail_repository") tvRepository: MediaRepository,
    private val savedStateHandle: SavedStateHandle
): ViewModel() {
    private val mediaId: String
    private val _videos: MutableLiveData<List<Video>> = MutableLiveData()
    private val _credits: MutableLiveData<Credits> = MutableLiveData()
    private val _recommendations: MutableLiveData<List<Media>> = MutableLiveData()
    private val _details: MutableLiveData<MediaDetail> = MutableLiveData()
    private val _movie: MutableLiveData<Media> = MutableLiveData()
    val videos by lazy {
        obtainVideos()
        _videos
    }

    val credits by lazy {
        obtainCredits()
        _credits
    }

    val recommendations by lazy {
        obtainRecommendations()
        _recommendations
    }

    val details by lazy {
        obtainDetails()
        _details
    }

    val media by lazy {
        obtainMedia()
        _movie
    }

    val mediaRepository: MediaRepository by lazy {
        if (savedStateHandle.get<String>(FRAGMENT_TYPE_KEY) == TV_TYPE){
            tvRepository
        } else {
            movieRepository
        }
    }

    val compositeDisposable: CompositeDisposable = CompositeDisposable()

    init {
        mediaId = savedStateHandle["media_id"]?:"0"
    }

    private fun obtainVideos() {
        compositeDisposable += mediaRepository.mediaVideos(mediaId)
            .applyMainSchedulers()
            .subscribe({ videos ->
                _videos.value = videos
            }, {})
    }

    private fun obtainCredits() {
        compositeDisposable += mediaRepository.mediaCredits(mediaId)
            .applyMainSchedulers()
            .subscribe({ credits ->
                _credits.value = credits
            }, {})
    }

    private fun obtainRecommendations() {
        compositeDisposable += mediaRepository.mediaRecommendations(mediaId)
            .applyMainSchedulers()
            .subscribe({ recommendations ->
                _recommendations.value = recommendations
            }, {})
    }

    private fun obtainDetails() {
        compositeDisposable += mediaRepository.mediaDetails(mediaId)
            .applyMainSchedulers()
            .subscribe({ movieDetail ->
                _details.value = movieDetail
            }, {})
    }

    private fun obtainMedia(){
        compositeDisposable += mediaRepository.mediaMainData(mediaId)
            .applyMainSchedulers()
            .subscribe({ movie ->
                _movie.value = movie
            }, {})
    }
}