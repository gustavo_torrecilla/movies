package com.movietime.detail.model

import com.google.gson.annotations.SerializedName
import com.movietime.media_model.Genre

interface MediaDetail {

    val backdropPath: String?

    val genres: List<Genre>?

    val homepage: String?

    val id: Int?

    val overview: String?

    val popularity: Double?

    val posterPath: String?

    val status: String?

    val voteAverage: Double?

    val voteCount: Int?
}