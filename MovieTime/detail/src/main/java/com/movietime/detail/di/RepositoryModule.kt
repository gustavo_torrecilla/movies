package com.movietime.detail.di

import com.movietime.detail.repositories.MediaRepository
import com.movietime.detail.repositories.MovieRepository
import com.movietime.detail.repositories.TvRepository
import dagger.Binds
import dagger.Module
import dagger.Reusable
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import javax.inject.Named

@Module
@InstallIn(ViewModelComponent::class)
abstract class RepositoryModule {
    @Binds
    @Reusable
    @Named("movie_detail_repository")
    abstract fun bindMovieRepository(movieRepository: MovieRepository): MediaRepository

    @Binds
    @Reusable
    @Named("tv_detail_repository")
    abstract fun bindTvRepository(tvRepository: TvRepository): MediaRepository
}