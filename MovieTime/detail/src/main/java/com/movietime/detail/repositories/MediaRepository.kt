package com.movietime.detail.repositories

import com.movietime.detail.model.MediaDetail
import com.movietime.detail.model.MovieDetail
import com.movietime.media_model.Credits
import com.movietime.media_model.Media
import com.movietime.media_model.Movie
import com.movietime.media_model.Video
import io.reactivex.Single

interface MediaRepository {
    fun mediaVideos(movieId: String): Single<List<Video>>

    fun mediaCredits(movieId: String): Single<Credits>

    fun mediaRecommendations(movieId: String): Single<List<Media>>

    fun mediaDetails(movieId: String): Single<MediaDetail>

    fun mediaMainData(mediaId: String): Single<Media>
}