package com.movietime.detail.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.movietime.detail.R
import com.movietime.media_model.Media

/**
 * Created by gustavo on 04/04/18.
 */
class MediaAdapter(private val mediaList: List<Media>, private val context: Context?, private val onItemClickListener: ((media: Media, transitionViews: Array<View>)->(Unit))) : RecyclerView.Adapter<MediaAdapter.ViewHolder>() {

    inner class ViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageView: ImageView
        val rate: TextView
        val cardView: CardView
        val gradient: View

        init {
            imageView = itemView.findViewById(R.id.image)
            rate = itemView.findViewById(R.id.rate)
            cardView = itemView.findViewById(R.id.cardView)
            gradient = itemView.findViewById(R.id.gradient)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val mediaView = inflater.inflate(R.layout.card_item, parent, false)
        (mediaView.findViewById<View>(R.id.cardView) as CardView).preventCornerOverlap = false
        return ViewHolder(mediaView)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val media = mediaList[position]
        viewHolder.rate.text = media.rating
        initializeImage(viewHolder, media)
        initializeTransitionNames(viewHolder, media)
        initializeCardView(viewHolder, media)
    }

    private fun initializeTransitionNames(viewHolder: ViewHolder, media: Media) {
        context?.let {
            val resources = context.resources
            viewHolder.imageView.transitionName =
                resources.getString(R.string.poster_transition) + media.id
            viewHolder.gradient.transitionName =
                resources.getString(R.string.gradient_poster_transition) + media.id
            viewHolder.rate.transitionName =
                resources.getString(R.string.rate_transition) + media.id
            viewHolder.cardView.transitionName =
                resources.getString(R.string.card_transition) + media.id
        }
    }

    private fun initializeCardView(viewHolder: ViewHolder, media: Media) {
        val transitionViews = arrayOf(viewHolder.cardView, viewHolder.gradient, viewHolder.rate, viewHolder.imageView)
        viewHolder.cardView.setOnClickListener { onItemClickListener.invoke(media, transitionViews) }
    }

    private fun initializeImage(viewHolder: ViewHolder, media: Media) {
        context?.let {
            Glide.with(context)
                .load(media.getPosterUrl())
                .into(viewHolder.imageView)
        }
    }

    override fun getItemCount(): Int {
        return mediaList.size
    }

}