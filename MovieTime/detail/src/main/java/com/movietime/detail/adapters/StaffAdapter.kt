package com.movietime.detail.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.movietime.detail.R
import com.movietime.media_model.Staff

class StaffAdapter(
    private val staffList: List<Staff>,
    private val context: Context?
) : RecyclerView.Adapter<StaffAdapter.ViewHolder>() {

    inner class ViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageView: ImageView = itemView.findViewById(R.id.image)
        var name: TextView = itemView.findViewById(R.id.name)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val crewView = inflater.inflate(R.layout.credit_item, parent, false)
        (crewView.findViewById<View>(R.id.cardView) as CardView).preventCornerOverlap = false
        return ViewHolder(crewView)
    }

    override fun onBindViewHolder(
        viewHolder: ViewHolder,
        position: Int
    ) {
        val staff = staffList[position]
        viewHolder.name.text = staff.name + " / " + staff.jobCharacter
        initializeImage(viewHolder, staff)
    }

    private fun initializeImage(
        viewHolder: ViewHolder,
        staff: Staff
    ) {
        context?.let {
            Glide.with(context)
                .load(staff.profileImage)
                .apply(
                    RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.ic_person)
                )
                .into(viewHolder.imageView!!)
        }

    }

    override fun getItemCount(): Int {
        return staffList.size
    }
}