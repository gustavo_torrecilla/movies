package com.movietime.detail.repositories

import com.movietime.database.dao.MovieDao
import com.movietime.detail.model.MediaDetail
import com.movietime.detail.services.MovieService
import com.movietime.media_model.Credits
import com.movietime.media_model.Movie
import com.movietime.detail.model.MovieDetail
import com.movietime.media_model.Media
import com.movietime.media_model.Video
import io.reactivex.Single
import javax.inject.Inject

class MovieRepository @Inject constructor(
    private val movieService: MovieService,
    private val movieDao: MovieDao
): MediaRepository {

    override fun mediaVideos(movieId: String): Single<List<Video>> {
        return movieService.movieVideos(movieId)
    }

    override fun mediaCredits(movieId: String): Single<Credits> {
        return movieService.movieCredits(movieId)
    }

    override fun mediaRecommendations(movieId: String): Single<List<Media>> {
        return movieService.movieRecommendations(movieId).map { it }
    }

    override fun mediaDetails(movieId: String): Single<MediaDetail> {
        return movieService.movieDetails(movieId).map { it }
    }

    override fun mediaMainData(mediaId: String): Single<Media> {
        return movieDao.getMovie(mediaId).map { it }
    }


}