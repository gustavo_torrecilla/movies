package com.movietime.detail.services

import com.movietime.media_model.Credits
import com.movietime.media_model.Movie
import com.movietime.detail.model.MovieDetail
import com.movietime.media_model.Video
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface MovieService {
    @GET("movie/{movie_id}/videos")
    fun movieVideos(@Path("movie_id") movieId: String?): Single<List<Video>>

    @GET("movie/{movie_id}/credits")
    fun movieCredits(@Path("movie_id") movieId: String?): Single<Credits>

    @GET("movie/{movie_id}/recommendations")
    fun movieRecommendations(@Path("movie_id") movieId: String?): Single<List<Movie>>

    @GET("movie/{movie_id}")
    fun movieDetails(@Path("movie_id") movieId: String?): Single<MovieDetail>
}