package com.movietime.detail.model

import com.google.gson.annotations.SerializedName
import com.movietime.media_model.Genre

class MovieDetail: MediaDetail {
    @SerializedName("adult")
    val adult: Boolean? = null

    @SerializedName("backdrop_path")
    override val backdropPath: String? = null

    @SerializedName("belongs_to_collection")
    val belongsToCollection: Any? = null

    @SerializedName("budget")
    val budget: Int? = null

    @SerializedName("genres")
    override val genres: List<Genre>? = null

    @SerializedName("homepage")
    override val homepage: String? = null

    @SerializedName("id")
    override val id: Int? = null

    @SerializedName("imdb_id")
    val imdbId: String? = null

    @SerializedName("original_language")
    val originalLanguage: String? = null

    @SerializedName("original_title")
    val originalTitle: String? = null

    @SerializedName("overview")
    override val overview: String? = null

    @SerializedName("popularity")
    override val popularity: Double? = null

    @SerializedName("poster_path")
    override val posterPath: String? = null

    @SerializedName("release_date")
    val releaseDate: String? = null

    @SerializedName("revenue")
    val revenue: Int? = null

    @SerializedName("runtime")
    val runtime: Int? = null

    @SerializedName("status")
    override val status: String? = null

    @SerializedName("tagline")
    val tagline: String? = null

    @SerializedName("title")
    val title: String? = null

    @SerializedName("video")
    val video: Boolean? = null

    @SerializedName("vote_average")
    override val voteAverage: Double? = null

    @SerializedName("vote_count")
    override val voteCount: Int? = null
}