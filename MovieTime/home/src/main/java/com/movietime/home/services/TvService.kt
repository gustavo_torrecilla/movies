package com.movietime.home.services

import com.movietime.media_model.Tv
import io.reactivex.Single
import retrofit2.http.GET

interface TvService {
    @GET("tv/top_rated")
    fun topRatedTvShows(): Single<List<Tv>>

    @GET("tv/on_the_air")
    fun onAirTvShows(): Single<List<Tv>>

    @GET("tv/popular")
    fun popularTvShows(): Single<List<Tv>>
}