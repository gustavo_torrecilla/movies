package com.movietime.home.services

import com.movietime.media_model.Movie
import io.reactivex.Single
import retrofit2.http.GET

interface MovieService {
    @GET("movie/top_rated")
    fun topRatedMovies(): Single<List<Movie>>

    @GET("movie/upcoming")
    fun upcomingMovies(): Single<List<Movie>>

    @GET("movie/popular")
    fun popularMovies(): Single<List<Movie>>
}