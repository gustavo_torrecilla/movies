package com.movietime.home.viewmodels

import android.view.View
import dagger.assisted.Assisted
import dagger.hilt.android.lifecycle.HiltViewModel
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.movietime.home.model.HomeActions
import com.movietime.home.model.MediaType
import com.movietime.media_model.Media
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

@HiltViewModel
class SharedViewModel @Inject constructor(): ViewModel() {
    private val _actions = PublishSubject.create<HomeActions>()
    val actions = _actions.hide()

    fun openDetail(media: Media, transitionViews: List<Pair<View, String>>, mediaType: MediaType) {
        _actions.onNext(HomeActions.OpenDetail(media, transitionViews, mediaType))
    }
}