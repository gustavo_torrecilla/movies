package com.movietime.home.repositories

import com.movietime.database.dao.MovieDao
import com.movietime.home.services.MovieService
import com.movietime.media_model.Media
import com.movietime.media_model.Movie
import io.reactivex.Single
import javax.inject.Inject

class MovieRepository @Inject constructor(
    private val movieDao: MovieDao,
    private val movieService: MovieService
): MediaRepository {

    override fun topRatedMedia(): Single<List<Media>> {
        return movieService.topRatedMovies()
            .map { list ->
                movieDao.insertMovies(list)
                list
            }
    }

    override fun upcomingMedia(): Single<List<Media>> {
        return movieService.upcomingMovies()
            .map { list ->
                movieDao.insertMovies(list)
                list
            }
    }

    override fun popularMedia(): Single<List<Media>> {
        return movieService.popularMovies()
            .map { list ->
                movieDao.insertMovies(list)
                list
            }
    }
}