package com.movietime.home.adapters

import android.content.Context
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.movietime.home.R
import com.movietime.media_model.Media

class MediaSliderAdapter(private val mediaList: List<Media>, context: Context?, onItemClickListener: (Media, Array<View>)->Unit) : PagerAdapter() {


    private val inflater: LayoutInflater
    private val context: Context?
    private val onItemClickListener: (Media, Array<View>)->Unit
    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = inflater.inflate(R.layout.media_pager_item, container, false)!!
        val backdrop = view.findViewById<ImageView>(R.id.backdrop)
        val poster = view.findViewById<ImageView>(R.id.posterImage)
        val cardView: CardView = view.findViewById(R.id.cardView)
        val name = view.findViewById<TextView>(R.id.name)
        val rating = view.findViewById<TextView>(R.id.rating)
        val overview = view.findViewById<TextView>(R.id.overview)
        val detail = view.findViewById<Button>(R.id.detail)
        val media = mediaList[position]
        initializeTransitionNames(poster, cardView, media)
        val transitionViews = arrayOf(poster, cardView)
        detail.setOnClickListener { onItemClickListener.invoke(media, transitionViews) }
        context?.let { context ->
            Glide.with(context)
                .load(media.getBackdropUrl())
                .into(backdrop)
            Glide.with(context)
                .load(media.getPosterUrl())
                .into(poster)
        }
        name.text = media.name
        rating.text = media.rating
        overview.text = media.overview
        container.addView(view, 0)
        return view
    }

    private fun initializeTransitionNames(poster: View, cardView: View, media: Media) {
        context?.let {
            val resources = context.resources
            poster.transitionName = resources.getString(R.string.poster_transition) + media.id
            cardView.transitionName = resources.getString(R.string.card_transition) + media.id
        }
    }

    override fun getCount(): Int {
        return mediaList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}
    override fun saveState(): Parcelable? {
        return null
    }

    init {
        inflater = LayoutInflater.from(context)
        this.context = context
        this.onItemClickListener = onItemClickListener
    }
}