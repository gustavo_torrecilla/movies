package com.movietime.home.model

import android.view.View
import com.movietime.media_model.Media

sealed class HomeActions {
    data class OpenDetail(val media: Media, val transitionViews: List<Pair<View, String>>, val mediaType: MediaType): HomeActions()
}

enum class MediaType {
    MOVIE,
    TV
}