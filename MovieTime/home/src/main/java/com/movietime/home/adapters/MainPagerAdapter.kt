package com.movietime.home.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.movietime.home.fragments.MediaFragment


class MainPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {
    override fun getCount(): Int {
        return NUM_ITEMS
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> MediaFragment.createMovieFragment()
            1 -> MediaFragment.createTvFragment()
            else -> MediaFragment.createMovieFragment()
        }
    }

    companion object {
        private const val NUM_ITEMS = 3
    }
}