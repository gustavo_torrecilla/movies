package com.movietime.home.repositories

import com.movietime.database.dao.TvDao
import com.movietime.home.services.TvService
import com.movietime.media_model.Media
import com.movietime.media_model.Tv
import io.reactivex.Single
import javax.inject.Inject

class TvRepository @Inject constructor(
    private val tvDao: TvDao,
    private val tvService: TvService
): MediaRepository {

    override fun topRatedMedia(): Single<List<Media>> {
        return tvService.topRatedTvShows()
            .map { list ->
                tvDao.insertTvShows(list)
                list
            }
    }

    override fun upcomingMedia(): Single<List<Media>> {
        return tvService.onAirTvShows()
            .map { list ->
                tvDao.insertTvShows(list)
                list
            }
    }

    override fun popularMedia(): Single<List<Media>> {
        return tvService.popularTvShows()
            .map { list ->
                tvDao.insertTvShows(list)
                list
            }
    }
}