package com.movietime.home.repositories

import com.movietime.media_model.Media
import com.movietime.media_model.Movie
import io.reactivex.Single

interface MediaRepository {
    fun topRatedMedia(): Single<List<Media>>
    fun upcomingMedia(): Single<List<Media>>
    fun popularMedia(): Single<List<Media>>
}