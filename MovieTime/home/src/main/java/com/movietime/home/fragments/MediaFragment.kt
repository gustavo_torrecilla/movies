package com.movietime.home.fragments

import android.os.Bundle
import android.os.Handler
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.doOnPreDraw
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.TransitionInflater
import com.movietime.home.R
import com.movietime.home.adapters.MediaAdapter
import com.movietime.home.adapters.MediaSliderAdapter
import com.movietime.home.databinding.MediaFragmentBinding
import com.movietime.home.model.MediaType
import com.movietime.home.view.DepthPageTransformer
import com.movietime.home.view.HorizontalSpaceItemDecoration
import com.movietime.home.viewmodels.MediaHomeViewModel
import com.movietime.home.viewmodels.SharedViewModel
import com.movietime.media_model.Media
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class MediaFragment: Fragment() {
    private val mediaHomeViewModel: MediaHomeViewModel by viewModels()
    private val sharedViewModel: SharedViewModel by activityViewModels()
    private lateinit var binding: MediaFragmentBinding

    //todo improve viewpager scroll
    private var timer: Timer? = null
    private var handler: Handler? = null
    private var pagerRunnable: Runnable? = null

    companion object {
        const val FRAGMENT_TYPE_KEY = "fragment_type_key"
        const val TV_TYPE = "tv"
        const val MOVIE_TYPE = "movie"

        fun createTvFragment() = createFragment(TV_TYPE)

        fun createMovieFragment() = createFragment(MOVIE_TYPE)

        private fun createFragment(fragmentType: String): MediaFragment {
            return MediaFragment().apply {
                arguments = Bundle().apply {
                    putString(FRAGMENT_TYPE_KEY, fragmentType)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        reenterTransition = TransitionInflater.from(context).inflateTransition(R.transition.home_reenter)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MediaFragmentBinding.inflate(inflater, container, false)
        initializeRecyclerView()
        initializeViewPager()
        observeLists()
        return binding.root
    }

    private fun observeLists(){
        mediaHomeViewModel.topRatedList.observe(viewLifecycleOwner, Observer { list ->
            val adapter = MediaAdapter(list, context) {media, transitionViews ->
                sharedViewModel.openDetail(media, transitionViews.map { transitionView -> Pair(transitionView, transitionView.transitionName) }, getType())
            }
            binding.rvTopRatedMedia.adapter = adapter
        })

        mediaHomeViewModel.extraList.observe(viewLifecycleOwner, Observer { list ->
            val adapter = MediaAdapter(list, context) {media, transitionViews ->
                sharedViewModel.openDetail(media, transitionViews.map { transitionView -> Pair(transitionView, transitionView.transitionName) }, getType())
            }
            binding.rvExtraListMedia.adapter = adapter
        })

        mediaHomeViewModel.popularList.observe(viewLifecycleOwner, Observer { list ->
            binding.vpPopularMedia.adapter = MediaSliderAdapter(list, context) { media: Media, transitionViews: Array<View> ->
                sharedViewModel.openDetail(media, transitionViews.map { transitionView -> Pair(transitionView, transitionView.transitionName) }, getType())
            }
        })
    }

    private fun getType(): MediaType = if(arguments?.get(FRAGMENT_TYPE_KEY) == TV_TYPE){
        MediaType.TV
    } else {
        MediaType.MOVIE
    }


    private fun initializeRecyclerView() {
        val space = Math.round(
            TypedValue
            .applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5f, resources.displayMetrics))
        val topRatedLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.rvTopRatedMedia.addItemDecoration(HorizontalSpaceItemDecoration(space))
        binding.rvTopRatedMedia.layoutManager = topRatedLayoutManager
        val upcomingLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.rvExtraListMedia.addItemDecoration(HorizontalSpaceItemDecoration(space))
        binding.rvExtraListMedia.layoutManager = upcomingLayoutManager
    }

    private fun initializeViewPager() {
        binding.vpPopularMedia.setPageTransformer(true, DepthPageTransformer())
        binding.vpPopularMedia.offscreenPageLimit = 2
        handler = Handler()
        pagerRunnable = Runnable {
            val pagerAdapter = binding.vpPopularMedia.adapter
            if (pagerAdapter != null &&
                binding.vpPopularMedia.currentItem != pagerAdapter.count - 1) binding.vpPopularMedia.setCurrentItem(binding.vpPopularMedia.currentItem + 1, true)
        }
    }

    override fun onPause() {
        super.onPause()
        cancelAutomaticPagerScroll()
    }

    override fun onResume() {
        super.onResume()
        startAutomaticPagerScroll()
    }

    private fun startAutomaticPagerScroll() {
        timer = Timer()
        timer!!.schedule(object : TimerTask() {
            override fun run() {
                handler!!.post(pagerRunnable)
            }
        }, 7000, 7000)
    }

    private fun cancelAutomaticPagerScroll() {
        timer!!.cancel()
        timer!!.purge()
    }
}