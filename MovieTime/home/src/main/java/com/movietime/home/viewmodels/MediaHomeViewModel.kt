package com.movietime.home.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.movietime.base.applyMainSchedulers
import com.movietime.base.plusAssign
import com.movietime.home.fragments.MediaFragment.Companion.FRAGMENT_TYPE_KEY
import com.movietime.home.fragments.MediaFragment.Companion.TV_TYPE
import com.movietime.home.repositories.MediaRepository
import com.movietime.media_model.Media
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject
import javax.inject.Named

@HiltViewModel
class MediaHomeViewModel @Inject constructor(
    @Named("movie_repository") movieRepository: MediaRepository,
    @Named("tv_repository") tvRepository: MediaRepository,
    private val savedStateHandle: SavedStateHandle
): ViewModel() {
    private val _topRatedList: MutableLiveData<List<Media>> = MutableLiveData()
    private val _popularList: MutableLiveData<List<Media>> = MutableLiveData()
    private val _extraList: MutableLiveData<List<Media>> = MutableLiveData()

    private val mediaRepository: MediaRepository by lazy {
        if (savedStateHandle.get<String>(FRAGMENT_TYPE_KEY) == TV_TYPE){
            tvRepository
        } else {
            movieRepository
        }
    }

    val topRatedList: LiveData<List<Media>> by lazy {
        obtainTopRatedList()
        _topRatedList
    }
    val popularList: LiveData<List<Media>> by lazy {
        obtainPopularList()
        _popularList
    }
    val extraList: LiveData<List<Media>> by lazy {
        obtainExtraList()
        _extraList
    }
    val compositeDisposable: CompositeDisposable = CompositeDisposable()

    fun obtainTopRatedList() {
        compositeDisposable += mediaRepository.topRatedMedia()
            .applyMainSchedulers()
            .subscribe({_topRatedList.value = it}, { Log.e("error", "topRatedMovies", it)})
    }

    private fun obtainPopularList() {
        compositeDisposable += mediaRepository.popularMedia()
            .applyMainSchedulers()
            .subscribe({_popularList.value = it}, {})
    }

    private fun obtainExtraList() {
        compositeDisposable += mediaRepository.upcomingMedia()
            .applyMainSchedulers()
            .subscribe({_extraList.value = it}, {})
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}


