package com.movietime.home.di

import com.movietime.home.repositories.MediaRepository
import com.movietime.home.repositories.MovieRepository
import com.movietime.home.repositories.TvRepository
import dagger.Binds
import dagger.Module
import dagger.Reusable
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import javax.inject.Named

@Module
@InstallIn(ViewModelComponent::class)
abstract class RepositoryModule {
    @Binds
    @Reusable
    @Named("movie_repository")
    abstract fun bindMovieRepository(movieRepository: MovieRepository): MediaRepository

    @Binds
    @Reusable
    @Named("tv_repository")
    abstract fun bindTvRepository(tvRepository: TvRepository): MediaRepository
}