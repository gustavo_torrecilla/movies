package com.movietime.home.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.movietime.base.applyMainSchedulers
import com.movietime.base.plusAssign
import com.movietime.home.R
import com.movietime.home.adapters.MainPagerAdapter
import com.movietime.home.databinding.MainFragmentBinding
import com.movietime.home.model.HomeActions
import com.movietime.home.viewmodels.SharedViewModel
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.disposables.CompositeDisposable

@AndroidEntryPoint
class MainFragment: Fragment() {
    private lateinit var binding: MainFragmentBinding
    private val sharedViewModel: SharedViewModel by activityViewModels()
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(inflater, container, false)
        binding.fragmentContainer.offscreenPageLimit = 1
        binding.fragmentContainer.adapter = MainPagerAdapter(childFragmentManager)
        binding.bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.movie -> binding.fragmentContainer.currentItem = 0
                R.id.tv -> binding.fragmentContainer.currentItem = 1
                R.id.search -> binding.fragmentContainer.currentItem = 2
            }
            true
        }
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        listenEvents()
    }

    fun listenEvents() {
        compositeDisposable += sharedViewModel.actions
            .applyMainSchedulers()
            .subscribe({ homeAction ->
                when(homeAction){
                    is HomeActions.OpenDetail -> navigateToDetail(homeAction)
                }
            }, {
                Log.e("MainFragment", "action error", it)
            })
    }

    private fun navigateToDetail(homeAction: HomeActions.OpenDetail) {
        findNavController().navigate(
            MainFragmentDirections.actionMainFragmentToDetailNavGraph(
                homeAction.media.id,
                homeAction.mediaType.name.lowercase()
            )
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }
}