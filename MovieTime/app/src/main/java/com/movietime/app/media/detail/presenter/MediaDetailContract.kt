package com.movietime.app.media.detail.presenter

import android.content.Context
import com.movietime.app.media.model.Cast
import com.movietime.app.media.model.Crew
import com.movietime.app.media.model.Genre
import com.movietime.app.media.model.Media
import com.movietime.app.media.model.Video

interface MediaDetailContract {
    interface View {
        fun showVideoList(video: List<Video>)
        fun showYoutubeVideo(video: Video)
        fun showRecommendations(recommendations: List<Media>)
        fun showGenres(genres: List<Genre>)
        fun showCast(cast: List<Cast>)
        fun showCrew(crew: List<Crew>)
        fun getMediaContext(): Context?
    }

    interface MovieView : View {
        fun showTagLine(tagline: String?)
        fun goToMovieDetailActivity(media: Media, transitionViews: Array<android.view.View>)
    }

    interface TvView : View {
        fun goToTvDetailActivity(media: Media, transitionViews: Array<android.view.View>)
    }

    interface Presenter {
        fun start(mediaId: String)
        fun onStartTransition()
        fun onFinishTransition()
        fun onRecommendationClicked(media: Media, transitionViews: Array<android.view.View>)
        fun onVideoChose(video: Video)
        fun onDestroy()
    }

    interface MoviePresenter : Presenter {
        fun attach(movieView: MovieView)
    }

    interface TvPresenter : Presenter {
        fun attach(tvView: TvView)
    }
}