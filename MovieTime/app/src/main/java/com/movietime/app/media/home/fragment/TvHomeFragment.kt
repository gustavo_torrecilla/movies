package com.movietime.app.media.home.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.movietime.app.R
import com.movietime.app.media.detail.activity.TvDetailActivity
import com.movietime.app.media.home.presenter.MediaHomeContract
import com.movietime.app.media.list.activity.TvListActivity
import com.movietime.app.media.model.Media
import javax.inject.Inject

class TvHomeFragment : MediaHomeFragment(), MediaHomeContract.TvView {

    @Inject
    override lateinit var presenter: MediaHomeContract.TvPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun setExtraListTitle() {
        binding.extraListTitle.setText(R.string.on_air)
    }

    override fun initializeMediaHomePresenter() {
        presenter.attach(this)
    }

    override fun goToTvListActivity(mediaList: List<Media>, title: String?) {
        val tvListIntent = Intent(context, TvListActivity::class.java)
        startListActivity(tvListIntent, mediaList, title)
    }

    override fun goToTvDetailActivity(media: Media, transitionViews: Array<View>) {
        val intent = Intent(context, TvDetailActivity::class.java)
        startDetailActivity(intent, media, transitionViews)
    }

    companion object {
        fun newInstance(): TvHomeFragment {
            return TvHomeFragment()
        }
    }
}