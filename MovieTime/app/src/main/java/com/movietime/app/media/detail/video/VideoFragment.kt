// package com.movietime.network.media.detail.video
//
// import android.os.Bundle
// import android.view.LayoutInflater
// import android.view.View
// import android.view.ViewGroup
// import com.google.android.youtube.player.YouTubeInitializationResult
// import com.google.android.youtube.player.YouTubePlayer
// import com.google.android.youtube.player.YouTubePlayerSupportFragment
//
// class VideoFragment : YouTubePlayerSupportFragment(),
//     YouTubePlayer.OnInitializedListener {
//     private var player: YouTubePlayer? = null
//     private var videoId: String? = null
//     override fun onCreate(savedInstanceState: Bundle?) {
//         super.onCreate(savedInstanceState)
//         initialize(DEVELOPER_KEY, this)
//     }
//
//     override fun onCreateView(
//         layoutInflater: LayoutInflater,
//         viewGroup: ViewGroup?,
//         bundle: Bundle?
//     ): View? {
//         val view = super.onCreateView(layoutInflater, viewGroup, bundle)
//         if (arguments != null) {
//             val videoId = arguments!!.getString(VIDEO_ID)
//             if (videoId != null && videoId != this.videoId) {
//                 this.videoId = videoId
//                 if (player != null) {
//                     player!!.cueVideo(videoId)
//                 }
//             }
//         }
//         return view
//     }
//
//     override fun onDestroy() {
//         if (player != null) {
//             player!!.release()
//         }
//         super.onDestroy()
//     }
//
//     fun finish() {
//         if (player != null) {
//             player!!.release()
//         }
//     }
//
//     fun setVideoId(videoId: String?) {
//         if (videoId != null && videoId != this.videoId) {
//             this.videoId = videoId
//             if (player != null) {
//                 player!!.cueVideo(videoId)
//             }
//         }
//     }
//
//     override fun onInitializationSuccess(
//         provider: YouTubePlayer.Provider,
//         player: YouTubePlayer,
//         restored: Boolean
//     ) {
//         this.player = player
//         if (!restored && videoId != null) {
//             player.cueVideo(videoId)
//         }
//     }
//
//     override fun onInitializationFailure(
//         provider: YouTubePlayer.Provider,
//         result: YouTubeInitializationResult
//     ) {
//         player = null
//     }
//
//     companion object {
//         private const val VIDEO_ID = "video_id"
//         private const val DEVELOPER_KEY = "AIzaSyA_apX-RJIwIH6rRKaBZSfmXpzWoPUI254"
//         fun newInstance(): VideoFragment {
//             val args = Bundle()
//             val fragment = VideoFragment()
//             fragment.arguments = args
//             return fragment
//         }
//     }
// }