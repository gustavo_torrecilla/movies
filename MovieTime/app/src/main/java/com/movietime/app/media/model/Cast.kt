package com.movietime.app.media.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.movietime.app.media.detail.adapter.StaffAdapter.Staff

class Cast : Staff {
    @SerializedName("character")
    @Expose
    override var jobCharacter: String? = null
        get() = field
        set

    @SerializedName("credit_id")
    @Expose
    var creditId: String? = null

    @SerializedName("gender")
    @Expose
    var gender: Int? = null

    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("name")
    @Expose
    override var name: String? = null

    @SerializedName("order")
    @Expose
    var order: Int? = null

    @SerializedName("profile_path")
    @Expose
    private var profilePath: String? = null

    override val profileImage: String
        get() = "http://image.tmdb.org/t/p/w500$profilePath"

    fun setProfilePath(profilePath: String?) {
        this.profilePath = profilePath
    }
}