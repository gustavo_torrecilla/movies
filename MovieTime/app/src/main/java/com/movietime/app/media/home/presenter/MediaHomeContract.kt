package com.movietime.app.media.home.presenter

import android.content.Context
import com.movietime.app.media.model.Media

interface MediaHomeContract {
    interface View {
        fun showPopularMediaList(mediaList: List<Media>)
        fun showTopRatedMediaList(mediaList: List<Media>)
        fun showExtraMediaList(mediaList: List<Media>)
        fun getMediaContext(): Context?
    }

    interface MovieView : View {
        fun goToMovieListActivity(mediaList: List<Media>, title: String?)
        fun goToMovieDetailActivity(media: Media, transitionViews: Array<android.view.View>)
    }

    interface TvView : View {
        fun goToTvListActivity(mediaList: List<Media>, title: String?)
        fun goToTvDetailActivity(media: Media, transitionViews: Array<android.view.View>)
    }

    interface Presenter {
        fun start()
        fun seeAllPopularClicked()
        fun seeAllTopRatedClicked()
        fun seeAllExtraListClicked()
        fun onListItemClicked(media: Media, transitionViews: Array<android.view.View>)
        fun onDestroy()
    }

    interface MoviePresenter : Presenter {
        fun attach(view: MovieView)
    }

    interface TvPresenter : Presenter {
        fun attach(view: TvView)
    }
}