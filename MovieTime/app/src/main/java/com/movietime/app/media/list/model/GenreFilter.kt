package com.movietime.app.media.list.model

import com.movietime.app.media.model.Media

abstract class GenreFilter {
    abstract fun filter(mediaList: List<Media>): List<Media>
}