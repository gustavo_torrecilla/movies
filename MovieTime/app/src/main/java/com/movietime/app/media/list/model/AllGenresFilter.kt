package com.movietime.app.media.list.model

import com.movietime.app.media.model.Media

class AllGenresFilter : GenreFilter() {
    override fun filter(mediaList: List<Media>): List<Media> {
        return mediaList
    }

    override fun toString(): String {
        return "All genres"
    }
}