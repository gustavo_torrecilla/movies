package com.movietime.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
open class MediaApplication : Application() {



}