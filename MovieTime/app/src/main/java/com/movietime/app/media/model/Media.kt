package com.movietime.app.media.model

import android.os.Parcel
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion
import com.google.gson.annotations.SerializedName

abstract class Media : SearchSuggestion {
    abstract val name: String
    abstract val isMovie: Boolean
    abstract val isTv: Boolean

    @SerializedName("id")
    lateinit var id: String

    @SerializedName("poster_path")
    var posterPath: String? = null

    @SerializedName("backdrop_path")
    var backdropPath: String? = null

    @SerializedName("vote_average")
    lateinit var rating: String

    @SerializedName("overview")
    lateinit var overview: String

    @SerializedName("genre_ids")
    lateinit var genreIds: List<Int>

    constructor() {}
    constructor(
        id: String,
        posterPath: String,
        backdropPath: String,
        rating: String,
        overview: String,
        genreIds: List<Int>
    ) {
        this.id = id
        this.posterPath = posterPath
        this.backdropPath = backdropPath
        this.rating = rating
        this.overview = overview
        this.genreIds = genreIds
    }

    fun getPosterUrl(): String { //todo sacar esto de aca
        return "http://image.tmdb.org/t/p/w500$posterPath"
    }

    fun getBackdropUrl(): String {
        return "http://image.tmdb.org/t/p/original$backdropPath"
    }

    override fun getBody(): String {
        return name
    }

    //  Parcelable
    protected constructor(`in`: Parcel) {
        id = `in`.readString()
        posterPath = `in`.readString()
        backdropPath = `in`.readString()
        rating = `in`.readString()
        overview = `in`.readString()
        genreIds = `in`.readArrayList(null) as List<Int>
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(id)
        dest.writeString(posterPath ?: "")
        dest.writeString(backdropPath ?: "")
        dest.writeString(rating)
        dest.writeString(overview)
        dest.writeList(genreIds)
    }

    override fun describeContents(): Int {
        return 0
    }
}