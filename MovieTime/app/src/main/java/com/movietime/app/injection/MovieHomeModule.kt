package com.movietime.app.injection

import com.movietime.app.media.home.presenter.MediaHomeContract
import com.movietime.app.media.home.presenter.MovieHomePresenter
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@Module
@InstallIn(ActivityComponent::class)
abstract class MovieHomeModule {

    @Binds
    abstract fun providePresenter(movieHomePresenter: MovieHomePresenter): MediaHomeContract.MoviePresenter

}