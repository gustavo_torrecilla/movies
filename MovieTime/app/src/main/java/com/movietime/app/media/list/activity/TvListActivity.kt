package com.movietime.app.media.list.activity

import android.content.Intent
import android.view.View
import com.movietime.app.datasource.TvDataSource
import com.movietime.app.media.detail.activity.TvDetailActivity
import com.movietime.app.media.list.presenter.MediaListContract
import com.movietime.app.media.list.presenter.TvListPresenter
import com.movietime.app.media.model.Media

class TvListActivity : MediaListActivity(), MediaListContract.TvView {
    private var tvPresenter: MediaListContract.TvPresenter = TvListPresenter(TvDataSource())
    override fun initializePresenter() {
        tvPresenter = TvListPresenter(TvDataSource())
        tvPresenter.attach(this)
    }

    override val presenter: MediaListContract.Presenter
        protected get() = tvPresenter

    override fun goToTvDetailActivity(
        media: Media,
        transitionViews: Array<View>
    ) {
        val intent = Intent(this, TvDetailActivity::class.java)
        startDetailActivityWithTransition(media, transitionViews, intent)
    }
}