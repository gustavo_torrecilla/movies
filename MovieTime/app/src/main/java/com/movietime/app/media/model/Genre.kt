package com.movietime.app.media.model

import com.google.gson.annotations.SerializedName

class Genre(@field:SerializedName("id") var id: Int, @field:SerializedName("name") var name: String)