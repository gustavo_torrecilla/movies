package com.movietime.app.client.deserializers

import com.google.gson.Gson
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import com.movietime.app.media.model.Media
import com.movietime.app.media.model.Movie
import com.movietime.app.media.model.Tv
import java.lang.reflect.Type
import java.util.ArrayList

class MediaListDeserializer :
    JsonDeserializer<List<Media>> {
    @Throws(JsonParseException::class)
    override fun deserialize(
        json: JsonElement,
        typeOfT: Type,
        context: JsonDeserializationContext
    ): List<Media> {
        val result: MutableList<Media> =
            ArrayList()
        val jsonArray = json.asJsonObject["results"].asJsonArray
        val gson = Gson()
        for (jsonElement in jsonArray) {
            val mediaType =
                jsonElement.asJsonObject["media_type"].asString
            when (mediaType) {
                "tv" -> result.add(gson.fromJson(jsonElement, Tv::class.java))
                "movie" -> result.add(
                    gson.fromJson(
                        jsonElement,
                        Movie::class.java
                    )
                )
            }
        }
        return result
    }
}