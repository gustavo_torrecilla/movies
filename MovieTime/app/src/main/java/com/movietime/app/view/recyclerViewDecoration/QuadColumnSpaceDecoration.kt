package com.movietime.app.view.recyclerViewDecoration

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class QuadColumnSpaceDecoration(private val space: Int) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect, view: View, parent: RecyclerView,
        state: RecyclerView.State
    ) {
        outRect.bottom = space
        val position = parent.getChildAdapterPosition(view)
        if (position >= 0 && position <= 3) {
            outRect.top = space
        }
        if (position % 4 == 3) {
            outRect.right = space
            outRect.left = space / 2
        } else if (position % 4 == 0) {
            outRect.right = space / 2
            outRect.left = space
        } else {
            outRect.right = space / 2
            outRect.left = space / 2
        }
    }
}