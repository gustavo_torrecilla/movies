package com.movietime.app.media.list.presenter

import android.view.View
import com.movietime.app.datasource.TvDataSource
import com.movietime.app.media.model.Media

class TvListPresenter(tvDataSource: TvDataSource?) : MediaListPresenter(tvDataSource!!),
    MediaListContract.TvPresenter {
    private var view: MediaListContract.TvView? = null
    override fun attach(view: MediaListContract.TvView?) {
        this.view = view
    }

    override fun onListItemClicked(
        media: Media,
        transitionViews: Array<View>
    ) {
        view!!.goToTvDetailActivity(media, transitionViews)
    }

    override fun onDestroy() {
        view = null
    }

    override fun getView(): MediaListContract.View? {
        return view
    }
}