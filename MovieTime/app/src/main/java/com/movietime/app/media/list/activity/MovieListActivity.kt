package com.movietime.app.media.list.activity

import android.content.Intent
import android.view.View
import com.movietime.app.datasource.MovieDataSource
import com.movietime.app.media.detail.activity.MovieDetailActivity
import com.movietime.app.media.list.presenter.MediaListContract
import com.movietime.app.media.list.presenter.MovieListPresenter
import com.movietime.app.media.model.Media

class MovieListActivity : MediaListActivity(), MediaListContract.MovieView {
    var moviePresenter: MediaListContract.MoviePresenter = MovieListPresenter(MovieDataSource())
    override fun initializePresenter() {
        moviePresenter = MovieListPresenter(MovieDataSource())
        moviePresenter.attach(this)
    }

    override val presenter: MediaListContract.Presenter
        protected get() = moviePresenter

    override fun goToMovieDetailActivity(
        media: Media,
        transitionViews: Array<View>
    ) {
        val intent = Intent(this, MovieDetailActivity::class.java)
        startDetailActivityWithTransition(media, transitionViews, intent)
    }
}