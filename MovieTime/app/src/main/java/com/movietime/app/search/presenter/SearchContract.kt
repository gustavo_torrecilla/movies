package com.movietime.app.search.presenter

import com.movietime.app.media.model.Media

interface SearchContract {
    interface View {
        fun showSearchResult(searchResult: List<Media>)
        fun showSearchSuggestions(searchSuggestions: List<Media>)
        fun goToMovieDetailActivityWithTransition(media: Media, transitionViews: Array<android.view.View>)
        fun goToTvDetailActivityWithTransition(media: Media, transitionViews: Array<android.view.View>)
        fun goToMovieDetailActivity(media: Media)
        fun goToTvDetailActivity(media: Media)
    }

    interface Presenter {
        fun attach(view: View)
        fun submitSearch(query: String)
        fun onSearchSuggestions(query: String?)
        fun onListItemClicked(media: Media, transitionViews: Array<android.view.View>)
        fun onSuggestionClicked(media: Media)
    }
}