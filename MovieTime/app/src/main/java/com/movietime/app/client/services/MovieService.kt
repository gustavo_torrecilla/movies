package com.movietime.app.client.services

import com.movietime.app.media.model.Credits
import com.movietime.app.media.model.Genres
import com.movietime.app.media.model.Movie
import com.movietime.app.media.model.MovieDetail
import com.movietime.app.media.model.Video
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface MovieService {
    @GET("movie/top_rated")
    fun topRatedMovies(): Call<List<Movie>>

    @GET("movie/upcoming")
    fun upcomingMovies(): Call<List<Movie>>

    @GET("movie/popular")
    fun popularMovies(): Call<List<Movie>>

    @GET("movie/{movie_id}/videos")
    fun movieVideos(@Path("movie_id") movieId: String?): Call<List<Video>>

    @GET("movie/{movie_id}/credits")
    fun movieCredits(@Path("movie_id") movieId: String?): Call<Credits>

    @GET("movie/{movie_id}/recommendations")
    fun movieRecommendations(@Path("movie_id") movieId: String?): Call<List<Movie>>

    @GET("movie/{movie_id}")
    fun movieDetails(@Path("movie_id") movieId: String?): Call<MovieDetail>

    @GET("genre/movie/list")
    fun genres(): Call<Genres>
}