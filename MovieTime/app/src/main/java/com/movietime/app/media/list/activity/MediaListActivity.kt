package com.movietime.app.media.list.activity

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityOptionsCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.movietime.app.R
import com.movietime.app.databinding.MediaFragmentBinding
import com.movietime.app.databinding.MediaListBinding
import com.movietime.app.media.detail.activity.MediaDetailActivity
import com.movietime.app.media.list.adapter.MediaListAdapter
import com.movietime.app.media.list.model.GenreFilter
import com.movietime.app.media.list.presenter.MediaListContract
import com.movietime.app.media.model.Media
import com.movietime.app.view.recyclerViewDecoration.VerticalSpaceItemDecoration
import org.parceler.Parcels
import androidx.core.util.Pair
import androidx.core.view.GravityCompat

abstract class MediaListActivity : AppCompatActivity(), MediaListContract.View {

    protected lateinit var binding: MediaListBinding
    private var mediaListAdapter: MediaListAdapter? = null
    private lateinit var mediaList: List<Media>
    protected abstract fun initializePresenter()
    protected abstract val presenter: MediaListContract.Presenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MediaListBinding.inflate(layoutInflater)
        setContentView(binding.root)
        mediaList =
            Parcels.unwrap(
                intent.getParcelableExtra(LIST_KEY)
            )
        val title = intent.getStringExtra(TITLE_KEY)
        initializePresenter()
        initializeActionBar(title)
        initializeRecyclerView()
        initializeDrawerLayout()
        initializeSpinner()
        presenter.start()
    }

    private fun initializeActionBar(title: String) {
        setSupportActionBar(binding.toolbar as Toolbar?)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = title
    }

    private fun initializeRecyclerView() {
        val space = Math.round(
            TypedValue
                .applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    14f,
                    resources.displayMetrics
                )
        )
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(applicationContext, LinearLayoutManager.VERTICAL, false)
        binding.rvMediaList.addItemDecoration(VerticalSpaceItemDecoration(space))
        binding.rvMediaList.layoutManager = layoutManager
        mediaListAdapter = MediaListAdapter(mediaList, applicationContext) { media, transitionViews ->
                presenter.onListItemClicked(
                    media,
                    transitionViews
                )
            }
        binding.rvMediaList.adapter = mediaListAdapter
    }

    private fun initializeDrawerLayout() {
        binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        binding.drawerLayout.addDrawerListener(object : DrawerLayout.DrawerListener {
            override fun onDrawerSlide(
                view: View,
                v: Float
            ) {
            }

            override fun onDrawerOpened(view: View) {
                binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
            }

            override fun onDrawerClosed(view: View) {
                binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
            }

            override fun onDrawerStateChanged(i: Int) {}
        })
    }

    private fun initializeSpinner() {
        binding.genresSpinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View,
                i: Int,
                l: Long
            ) {
                val genreFilter = binding.genresSpinner.getItemAtPosition(i) as GenreFilter
                binding.drawerLayout.closeDrawer(GravityCompat.END)
                mediaListAdapter!!.setMediaList(genreFilter.filter(mediaList))
                mediaListAdapter!!.notifyDataSetChanged()
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.media_list_menu, menu)
        //change icon color
        val drawable = menu.findItem(R.id.filter).icon
        changeDrawableColor(drawable)
        return true
    }

    private fun changeDrawableColor(drawable: Drawable?) {
        if (drawable != null) {
            drawable.mutate()
            drawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.filter) {
            binding.drawerLayout.openDrawer(GravityCompat.END)
            return true
        } else if (item.itemId == android.R.id.home) {
            finishAfterTransition()
            return true
        }
        return false
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun showGenresFilter(genreFilters: List<GenreFilter>) {
        val genresAdapter: ArrayAdapter<*> =
            ArrayAdapter<Any?>(this, android.R.layout.simple_spinner_item, genreFilters)
        genresAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.genresSpinner.adapter = genresAdapter
    }

    override fun getContext(): Context {
        return applicationContext
    }

    protected fun startDetailActivityWithTransition(
        media: Media,
        transitionViews: Array<View>,
        intent: Intent
    ) {
        intent.putExtra(
            MediaDetailActivity.MEDIA_KEY,
            Parcels.wrap(media)
        )
        val pairList: Array<Pair<View, String>> = transitionViews.map { transitionView -> Pair(transitionView, transitionView.transitionName) }.toTypedArray()
        val options =
            ActivityOptionsCompat.makeSceneTransitionAnimation(this@MediaListActivity, *pairList)
        startActivity(intent, options.toBundle())
    }

    companion object {
        const val LIST_KEY = "list_key"
        const val TITLE_KEY = "title_key"
    }
}