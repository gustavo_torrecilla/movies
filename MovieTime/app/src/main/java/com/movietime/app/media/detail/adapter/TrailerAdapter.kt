// package com.movietime.network.media.detail.adapter
//
// import android.view.LayoutInflater
// import android.view.View
// import android.view.ViewGroup
// import androidx.constraintlayout.widget.ConstraintLayout
// import androidx.recyclerview.widget.RecyclerView
// import com.google.android.youtube.player.YouTubeInitializationResult
// import com.google.android.youtube.player.YouTubeThumbnailLoader
// import com.google.android.youtube.player.YouTubeThumbnailView
// import com.movietime.network.R
// import com.movietime.network.media.model.Video
// import java.util.HashMap
//
// class TrailerAdapter(
//     private val videoList: List<Video>,
//     var onItemClickListener: (Video) -> Unit = {_ ->}
// ) : RecyclerView.Adapter<TrailerAdapter.ViewHolder>() {
//     private var selectedPosition = 0
//
//     inner class ViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
//         var thumbnail: YouTubeThumbnailView = itemView.findViewById(R.id.thumbnail)
//         var container: ConstraintLayout = itemView.findViewById(R.id.container)
//         var opacity: View = itemView.findViewById(R.id.opacity)
//     }
//
//     private val thumbnailViewToLoaderMap: MutableMap<YouTubeThumbnailView?, YouTubeThumbnailLoader>
//     override fun onCreateViewHolder(
//         parent: ViewGroup,
//         viewType: Int
//     ): ViewHolder {
//         val context = parent.context
//         val inflater = LayoutInflater.from(context)
//         val crewView = inflater.inflate(R.layout.video_thumbnail, parent, false)
//         val viewHolder =
//             ViewHolder(crewView)
//         viewHolder.thumbnail.initialize(
//             DEVELOPER_KEY,
//             object : YouTubeThumbnailView.OnInitializedListener {
//                 override fun onInitializationSuccess(
//                     view: YouTubeThumbnailView,
//                     loader: YouTubeThumbnailLoader
//                 ) {
//                     thumbnailViewToLoaderMap[view] = loader
//                     val videoId = view.tag as String
//                     loader.setVideo(videoId)
//                 }
//
//                 override fun onInitializationFailure(
//                     youTubeThumbnailView: YouTubeThumbnailView,
//                     youTubeInitializationResult: YouTubeInitializationResult
//                 ) {
//                 }
//             })
//         return ViewHolder(crewView)
//     }
//
//     override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
//         val video = videoList[position]
//         if (position == selectedPosition) {
//             viewHolder.opacity.visibility = View.GONE
//         } else {
//             viewHolder.opacity.visibility = View.VISIBLE
//         }
//         viewHolder.container.setOnClickListener {
//             val oldSelectedPosition = selectedPosition
//             selectedPosition = viewHolder.adapterPosition
//             notifyItemChanged(oldSelectedPosition)
//             notifyItemChanged(selectedPosition)
//             onItemClickListener.invoke(video)
//         }
//         val loader = thumbnailViewToLoaderMap[viewHolder.thumbnail]
//         if (loader == null) {
//             viewHolder.thumbnail.tag = video.key
//         } else {
//             loader.setVideo(video.key)
//         }
//     }
//
//     fun releaseLoaders() {
//         for (loader in thumbnailViewToLoaderMap.values) {
//             loader.release()
//         }
//     }
//
//     override fun getItemCount(): Int {
//         return videoList.size
//     }
//
//     companion object {
//         private const val DEVELOPER_KEY = "AIzaSyA_apX-RJIwIH6rRKaBZSfmXpzWoPUI254"
//     }
//
//     init {
//         thumbnailViewToLoaderMap =
//             HashMap()
//     }
// }