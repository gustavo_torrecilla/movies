package com.movietime.app.media.detail.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.movietime.app.media.detail.presenter.MediaDetailContract
import com.movietime.app.media.model.Media
import javax.inject.Inject

class TvDetailActivity : MediaDetailActivity(), MediaDetailContract.TvView {

    @Inject
    lateinit var presenter: MediaDetailContract.TvPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    public override fun initializePresenter(): MediaDetailContract.Presenter {
        presenter.attach(this)
        return presenter
    }

    override fun goToTvDetailActivity(media: Media, transitionViews: Array<View>) {
        val intent = Intent(baseContext, TvDetailActivity::class.java)
        startDetailActivity(media, transitionViews, intent)
    }
}