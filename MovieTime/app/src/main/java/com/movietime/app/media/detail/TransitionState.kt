package com.movietime.app.media.detail

abstract class TransitionState internal constructor(protected var stateObserver: StateObserver) {
    abstract fun onFinishLoading()
    interface StateObserver {
        fun showInfo()
    }
}