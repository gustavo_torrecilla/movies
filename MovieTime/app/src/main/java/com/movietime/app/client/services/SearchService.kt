package com.movietime.app.client.services

import com.movietime.app.media.model.Media
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchService {
    @GET("search/multi")
    fun multiSearch(@Query("query") query: String?): Call<List<Media>>
}