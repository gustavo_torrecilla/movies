package com.movietime.app.media.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import org.parceler.Parcel

@Parcel
class Movie : Media {
    @SerializedName("title")
    override var name: String = ""

    //for parcel
    constructor() {}
    constructor(
        id: String?,
        posterUrl: String?,
        backdropUrl: String?,
        rating: String?,
        overview: String?,
        genreIds: List<Int>,
        name: String
    ) : super(id!!, posterUrl!!, backdropUrl!!, rating!!, overview!!, genreIds) {
        this.name = name
    }

    override val isMovie: Boolean
        get() = true

    override val isTv: Boolean
        get() = false

    private constructor(`in`: android.os.Parcel) : super(`in`) {
        name = `in`.readString()
    }

    override fun writeToParcel(dest: android.os.Parcel, flags: Int) {
        super.writeToParcel(dest, flags)
        dest.writeString(name)
    }

    companion object {
        //  Parcelable
        @JvmField
        val CREATOR: Parcelable.Creator<*> = object : Parcelable.Creator<Any?> {
            override fun createFromParcel(`in`: android.os.Parcel): Movie? {
                return Movie(`in`)
            }

            override fun newArray(size: Int): Array<Movie?> {
                return arrayOfNulls(size)
            }
        }
    }
}