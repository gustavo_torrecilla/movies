package com.movietime.app.media.detail.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.movietime.app.media.detail.presenter.MediaDetailContract
import com.movietime.app.media.model.Media
import javax.inject.Inject

class MovieDetailActivity : MediaDetailActivity(), MediaDetailContract.MovieView {

    @Inject
    lateinit var presenter: MediaDetailContract.MoviePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    public override fun initializePresenter(): MediaDetailContract.Presenter {
        presenter.attach(this)
        return presenter
    }

    override fun showTagLine(tagline: String?) {
        binding.tagline.text = tagline
        appearAnimation(binding.tagline)
    }

    override fun goToMovieDetailActivity(media: Media, transitionViews: Array<View>) {
        val intent = Intent(baseContext, MovieDetailActivity::class.java)
        startDetailActivity(media, transitionViews, intent)
    }
}