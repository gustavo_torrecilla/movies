package com.movietime.app.datasource

import com.movietime.app.client.ServiceGenerator
import com.movietime.app.client.services.SearchService
import com.movietime.app.media.model.Media
import javax.inject.Inject

class SearchDataSource @Inject constructor() {
    fun multiSearch(
        query: String?,
        dataSourceInterface: DataSourceInterface<List<Media>>
    ) {
        ServiceGenerator.createService(SearchService::class.java)
            .multiSearch(query)
            .enqueue(
                DataSourceCallback(
                    dataSourceInterface
                )
            )
    }
}