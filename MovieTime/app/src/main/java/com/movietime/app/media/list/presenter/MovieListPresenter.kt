package com.movietime.app.media.list.presenter

import android.view.View
import com.movietime.app.datasource.MovieDataSource
import com.movietime.app.media.model.Media

class MovieListPresenter(movieDataSource: MovieDataSource?) :
    MediaListPresenter(movieDataSource!!),
    MediaListContract.MoviePresenter {
    private var view: MediaListContract.MovieView? = null
    override fun attach(view: MediaListContract.MovieView?) {
        this.view = view
    }

    override fun onListItemClicked(
        media: Media,
        transitionViews: Array<View>
    ) {
        view!!.goToMovieDetailActivity(media, transitionViews)
    }

    override fun onDestroy() {
        view = null
    }

    override fun getView(): MediaListContract.View? {
        return view
    }
}