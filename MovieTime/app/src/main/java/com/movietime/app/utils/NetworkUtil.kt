package com.movietime.app.utils

import android.content.Context
import android.net.ConnectivityManager

object NetworkUtil {
    @JvmStatic
    fun hasInternetConnection(context: Context): Boolean {
        val cm =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                ?: return false
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null &&
            activeNetwork.isConnectedOrConnecting
    }
}