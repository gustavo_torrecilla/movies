package com.movietime.app.media.detail

class NoTransitionState(stateObserver: StateObserver) :
    TransitionState(stateObserver) {
    override fun onFinishLoading() {
        stateObserver.showInfo()
    }
}