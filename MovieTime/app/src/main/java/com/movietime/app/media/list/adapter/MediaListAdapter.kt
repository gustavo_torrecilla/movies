package com.movietime.app.media.list.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.movietime.app.R
import com.movietime.app.media.model.Media
import java.util.ArrayList

/**
 * Created by gustavo on 04/04/18.
 */
class MediaListAdapter(
    private var mediaList: List<Media>,
    private val context: Context?,
    val onItemClickListener: (media: Media, transitionViews: Array<View>) -> Unit
) : RecyclerView.Adapter<MediaListAdapter.ViewHolder>() {

    inner class ViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val backdrop: ImageView = itemView.findViewById(R.id.backdrop)

        val posterImage: ImageView = itemView.findViewById(R.id.posterImage)

        val rating: TextView = itemView.findViewById(R.id.rating)

        val name: TextView = itemView.findViewById(R.id.name)

        val overview: TextView = itemView.findViewById(R.id.overview)

        val cardView: CardView = itemView.findViewById(R.id.cardView)

        val itemCardView: CardView = itemView.findViewById(R.id.itemCardView)
    }

    constructor(
        context: Context?,
        onItemClickListener: (media: Media, transitionViews: Array<View>) -> Unit
    ) : this(
        ArrayList<Media>(),
        context,
        onItemClickListener
    ) {
    }

    fun setMediaList(mediaList: List<Media>) {
        this.mediaList = mediaList
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val mediaView = inflater.inflate(R.layout.media_list_item, parent, false)
        (mediaView.findViewById<View>(R.id.itemCardView) as CardView).preventCornerOverlap = false
        return ViewHolder(mediaView)
    }

    override fun onBindViewHolder(
        viewHolder: ViewHolder,
        position: Int
    ) {
        val media = mediaList[position]
        viewHolder.name!!.text = media.name
        viewHolder.overview!!.text = media.overview
        viewHolder.rating!!.text = media.rating
        initializeBackdrop(viewHolder, media)
        initializePoster(viewHolder, media)
        initializeTransitionNames(viewHolder, media)
        initializeCardView(viewHolder, media)
    }

    private fun initializeTransitionNames(
        viewHolder: ViewHolder,
        media: Media
    ) {
        val resources = context?.resources
        viewHolder.posterImage!!.transitionName =
            resources?.getString(R.string.poster_transition) + media.id
        viewHolder.cardView!!.transitionName =
            resources?.getString(R.string.card_transition) + media.id
    }

    private fun initializeCardView(
        viewHolder: ViewHolder,
        media: Media
    ) {
        viewHolder.itemCardView!!.setOnClickListener {
            val transitionViews =
                arrayOf(viewHolder.cardView, viewHolder.posterImage)
            onItemClickListener.invoke(media, transitionViews)
        }
    }

    private fun initializeBackdrop(
        viewHolder: ViewHolder,
        media: Media
    ) {
        context?.let {
            Glide.with(context)
                .load(media.getBackdropUrl())
                .into(viewHolder.backdrop!!)
        }
    }

    private fun initializePoster(
        viewHolder: ViewHolder,
        media: Media
    ) {
        context?.let {
            Glide.with(context)
                .load(media.getPosterUrl())
                .into(viewHolder.posterImage!!)
        }
    }

    override fun getItemCount(): Int {
        return mediaList.size
    }
}