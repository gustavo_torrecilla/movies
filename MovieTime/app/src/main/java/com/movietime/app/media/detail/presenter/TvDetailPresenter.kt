package com.movietime.app.media.detail.presenter

import android.view.View
import com.movietime.app.datasource.DataSourceInterface
import com.movietime.app.datasource.TvDataSource
import com.movietime.app.media.model.Media
import com.movietime.app.media.model.TvDetail
import javax.inject.Inject

class TvDetailPresenter @Inject constructor(private val tvDataSource: TvDataSource) : MediaDetailPresenter(tvDataSource), MediaDetailContract.TvPresenter {
    private var tvView: MediaDetailContract.TvView? = null
    private var tvDetail: TvDetail? = null
    override fun attach(tvView: MediaDetailContract.TvView) {
        this.tvView = tvView
    }

    override fun onRecommendationClicked(media: Media, transitionViews: Array<View>) {
        tvView?.goToTvDetailActivity(media, transitionViews)
    }

    override fun onDestroy() {
        tvView = null
    }

    override fun getView(): MediaDetailContract.View? {
        return tvView
    }

    public override fun obtainDetails(mediaId: String) {
        tvDataSource.getDetails(mediaId, tvView?.getMediaContext(), object : DataSourceInterface<TvDetail>() {
            override fun onSuccess(tvDetails: TvDetail) {
                if (getView() == null) return
                tvDetail = tvDetails
                transitionState.onFinishLoading()
            }
        })
    }

    override fun detailsFinishLoading(): Boolean {
        return tvDetail != null
    }

    override fun showDetails() {
        tvDetail?.genres?.let { if (it.isEmpty()) tvView?.showGenres(it) }
    }

}