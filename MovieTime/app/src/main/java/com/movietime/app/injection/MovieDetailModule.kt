package com.movietime.app.injection

import com.movietime.app.media.detail.activity.MovieDetailActivity
import com.movietime.app.media.detail.presenter.MediaDetailContract
import com.movietime.app.media.detail.presenter.MovieDetailPresenter
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(ActivityComponent::class)
abstract class MovieDetailModule {

    @Binds
    abstract fun providePresenter(movieDetailPresenter: MovieDetailPresenter): MediaDetailContract.MoviePresenter

}