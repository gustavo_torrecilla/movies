package com.movietime.app.injection

import com.movietime.app.media.list.activity.TvListActivity
import com.movietime.app.media.list.presenter.MediaListContract
import com.movietime.app.media.list.presenter.TvListPresenter
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(ActivityComponent::class)
abstract class TvListModule {
    @Binds
    abstract fun providePresenter(tvListPresenter: TvListPresenter): MediaListContract.TvPresenter
    
}