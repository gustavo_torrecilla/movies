package com.movietime.app.injection

import com.movietime.app.media.list.presenter.MediaListContract
import com.movietime.app.media.list.presenter.MovieListPresenter
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@Module
@InstallIn(ActivityComponent::class)
abstract class MovieListModule {

    @Binds
    abstract fun providePresenter(movieListPresenter: MovieListPresenter): MediaListContract.MoviePresenter

}