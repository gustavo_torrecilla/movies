package com.movietime.app.client.services

import com.movietime.app.media.model.Credits
import com.movietime.app.media.model.Genres
import com.movietime.app.media.model.Tv
import com.movietime.app.media.model.TvDetail
import com.movietime.app.media.model.Video
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface TvService {
    @GET("tv/top_rated")
    fun topRatedTvShows(): Call<List<Tv>>

    @GET("tv/on_the_air")
    fun onAirTvShows(): Call<List<Tv>>

    @GET("tv/popular")
    fun popularTvShows(): Call<List<Tv>>

    @GET("tv/{tvId}/videos")
    fun tvVideos(@Path("tvId") tvId: String?): Call<List<Video>>

    @GET("tv/{tvId}/credits")
    fun tvCredits(@Path("tvId") tvId: String?): Call<Credits>

    @GET("tv/{tvId}/recommendations")
    fun tvRecommendations(@Path("tvId") tvId: String?): Call<List<Tv>>

    @GET("tv/{tvId}")
    fun tvDetails(@Path("tvId") tvId: String?): Call<TvDetail>

    @GET("genre/tv/list")
    fun genres(): Call<Genres>
}