package com.movietime.app.client

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.movietime.app.client.deserializers.ListDeserializer
import com.movietime.app.client.deserializers.MediaListDeserializer
import com.movietime.app.client.interceptor.ApiKeyInterceptor
import com.movietime.app.client.interceptor.RewriteCacheInterceptor
import com.movietime.app.media.model.Media
import com.movietime.app.media.model.Movie
import com.movietime.app.media.model.Tv
import com.movietime.app.media.model.Video
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ServiceGenerator {
    private const val BASE_URL = "https://api.themoviedb.org/3/"
    private val builder = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(gson()))
    private val logging = HttpLoggingInterceptor()
        .setLevel(HttpLoggingInterceptor.Level.BODY)
    private val apiKey = ApiKeyInterceptor()
    private val httpClient = OkHttpClient.Builder()
    private val retrofit = retrofit()
    private var retrofitCache: Retrofit? = null
    private fun gson(): Gson {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.registerTypeAdapter(
            object :
                TypeToken<@JvmSuppressWildcards List<Movie>>() {}.type,
            ListDeserializer<Movie>()
        )
        gsonBuilder.registerTypeAdapter(
            object :
                TypeToken<@JvmSuppressWildcards List<Tv>>() {}.type,
            ListDeserializer<Tv>()
        )
        gsonBuilder.registerTypeAdapter(
            object :
                TypeToken<@JvmSuppressWildcards List<Video>>() {}.type,
            ListDeserializer<Video>()
        )
        gsonBuilder.registerTypeAdapter(
            object :
                TypeToken<@JvmSuppressWildcards List<Media>>() {}.type,
            MediaListDeserializer()
        )
        return gsonBuilder.create()
    }

    private fun retrofit(): Retrofit {
        httpClient.addInterceptor(apiKey)
        httpClient.addInterceptor(logging)
        builder.client(httpClient.build())
        return builder.build()
    }

    private fun retrofitWithCache(context: Context?): Retrofit {
        context?.let {
            val cacheSize = 10 * 1024 * 1024 // 10 MB
            val cache = Cache(context.cacheDir, cacheSize.toLong())
            httpClient.addInterceptor(
                RewriteCacheInterceptor(context)
            )
            httpClient.cache(cache)
        }
        builder.client(httpClient.build())
        return builder.build()
    }

    fun <S> createService(serviceClass: Class<S>?): S {
        return retrofit.create(serviceClass)
    }

    fun <S> createServiceWithCache(
        serviceClass: Class<S>?,
        context: Context?
    ): S {
        if (retrofitCache == null) retrofitCache =
            retrofitWithCache(context)
        return retrofitCache!!.create(serviceClass)
    }
}