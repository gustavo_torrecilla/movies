package com.movietime.app.media.home.presenter

import android.view.View
import com.movietime.app.R
import com.movietime.app.datasource.DataSourceInterface
import com.movietime.app.datasource.TvDataSource
import com.movietime.app.media.model.Media
import com.movietime.app.media.model.Tv
import javax.inject.Inject

class TvHomePresenter @Inject constructor(private val tvDataSource: TvDataSource):
        MediaHomePresenter(tvDataSource), MediaHomeContract.TvPresenter {
    private var view: MediaHomeContract.TvView? = null
    override fun attach(view: MediaHomeContract.TvView) {
        this.view = view
    }

    override fun onListItemClicked(media: Media, transitionViews: Array<View>) {
        view?.goToTvDetailActivity(media, transitionViews)
    }

    override fun seeAllPopularClicked() {
        popularList?.let {
            view?.goToTvListActivity(it, view?.getMediaContext()?.getString(R.string.popular))
        }
    }

    override fun seeAllTopRatedClicked() {
        topRatedList?.let {
            view?.goToTvListActivity(it, view?.getMediaContext()?.getString(R.string.top_rated))
        }
    }

    override fun seeAllExtraListClicked() {
        extraList?.let {
            view?.goToTvListActivity(it, view?.getMediaContext()?.getString(R.string.on_air))
        }
    }

    override fun onDestroy() {
        view = null
    }

    override fun obtainExtraList() {
        tvDataSource.onAirList(view?.getMediaContext(), object : DataSourceInterface<List<Tv>>() {
            override fun onSuccess(mediaList: List<Tv>) {
                extraList = mediaList
                if (view != null) view!!.showExtraMediaList(mediaList)
            }
        })
    }

    override fun getView(): MediaHomeContract.View? {
        return view
    }

}