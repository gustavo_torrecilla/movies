package com.movietime.app.injection

import com.movietime.app.media.home.presenter.MediaHomeContract
import com.movietime.app.media.home.presenter.TvHomePresenter
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@Module
@InstallIn(ActivityComponent::class)
abstract class TvHomeModule {

    @Binds
    abstract fun providePresenter(tvHomePresenter: TvHomePresenter): MediaHomeContract.TvPresenter

}