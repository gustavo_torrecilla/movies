package com.movietime.app.media.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.movietime.app.media.detail.adapter.StaffAdapter.Staff

class Crew : Staff {
    @SerializedName("credit_id")
    @Expose
    var creditId: String? = null

    @SerializedName("department")
    @Expose
    var department: String? = null

    @SerializedName("gender")
    @Expose
    var gender: Int? = null

    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("job")
    @Expose
    override var jobCharacter: String? = null
        get() = field
        set

    @SerializedName("name")
    @Expose
    override var name: String? = null

    @SerializedName("profile_path")
    @Expose
    private var profilePath: String? = null

    override val profileImage: String
        get() = "http://image.tmdb.org/t/p/w500$profilePath"

    fun setProfilePath(profilePath: String?) {
        this.profilePath = profilePath
    }
}