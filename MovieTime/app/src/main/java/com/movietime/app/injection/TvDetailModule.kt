package com.movietime.app.injection

import com.movietime.app.media.detail.presenter.MediaDetailContract
import com.movietime.app.media.detail.presenter.TvDetailPresenter
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@Module
@InstallIn(ActivityComponent::class)
abstract class TvDetailModule {

    @Binds
    abstract fun providePresenter(tvDetalPresenter: TvDetailPresenter): MediaDetailContract.TvPresenter

}