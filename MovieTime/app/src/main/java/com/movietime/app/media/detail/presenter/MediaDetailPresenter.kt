package com.movietime.app.media.detail.presenter

import com.movietime.app.datasource.DataSourceInterface
import com.movietime.app.datasource.MediaDataSource
import com.movietime.app.media.detail.NoTransitionState
import com.movietime.app.media.detail.TransitionRunningState
import com.movietime.app.media.detail.TransitionState
import com.movietime.app.media.detail.TransitionState.StateObserver
import com.movietime.app.media.model.Credits
import com.movietime.app.media.model.Media
import com.movietime.app.media.model.Video

abstract class MediaDetailPresenter(protected var mediaDataSource: MediaDataSource) : MediaDetailContract.Presenter, StateObserver {
    protected var transitionState: TransitionState
    private var videoList: List<Video>? = null
    private var credits: Credits? = null
    private var recommendations: List<Media>? = null
    protected abstract fun obtainDetails(mediaId: String)
    protected abstract fun detailsFinishLoading(): Boolean
    protected abstract fun showDetails()
    protected abstract fun getView(): MediaDetailContract.View?
    override fun start(mediaId: String) {
        if (getView() == null) return
        obtainVideos(mediaId)
        obtainCredits(mediaId)
        obtainRecommendations(mediaId)
        obtainDetails(mediaId)
    }

    private fun obtainVideos(mediaId: String) {
        mediaDataSource.getVideos(mediaId, getView()?.getMediaContext(), object : DataSourceInterface<List<Video>>() {
            override fun onSuccess(videos: List<Video>) {
                if (getView() == null) return
                videoList = videos
                transitionState.onFinishLoading()
            }
        })
    }

    private fun obtainCredits(mediaId: String) {
        mediaDataSource.getCredits(mediaId, getView()?.getMediaContext(), object : DataSourceInterface<Credits>() {
            override fun onSuccess(credit: Credits) {
                if (getView() == null) return
                credits = credit
                transitionState.onFinishLoading()
            }
        })
    }

    private fun obtainRecommendations(mediaId: String) {
        mediaDataSource.recommendations(mediaId, getView()?.getMediaContext(), object : DataSourceInterface<List<Media>>() {
            override fun onSuccess(recommendationList: List<Media>) {
                if (getView() == null) return
                recommendations = recommendationList
                transitionState.onFinishLoading()
            }
        })
    }

    override fun onVideoChose(video: Video) {
        getView()?.showYoutubeVideo(video)
    }

    override fun onStartTransition() {
        transitionState = TransitionRunningState(this)
    }

    override fun onFinishTransition() {
        transitionState = NoTransitionState(this)
        showInfo()
    }

    override fun showInfo() {
        if (detailsFinishLoading()) {
            recommendations?.let { if (it.isEmpty().not()) getView()?.showRecommendations(it) }
            videoList?.let { if (it.isEmpty().not()) getView()?.showVideoList(it) }
            showDetails()
            showCredits()
        }
    }

    private fun showCredits() {
        credits?.cast?.let { if (it.isEmpty().not()) getView()?.showCast(it) }
        credits?.crew?.let { if (it.isEmpty().not()) getView()?.showCrew(it) }
    }

    init {
        transitionState = NoTransitionState(this)
    }
}