package com.movietime.app.media.model

import com.google.gson.annotations.SerializedName

class Genres {
    @SerializedName("genres")
    lateinit var genres: List<Genre>
}