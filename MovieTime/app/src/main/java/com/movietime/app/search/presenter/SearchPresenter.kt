package com.movietime.app.search.presenter

import android.view.View
import com.movietime.app.datasource.DataSourceInterface
import com.movietime.app.datasource.SearchDataSource
import com.movietime.app.media.model.Media
import javax.inject.Inject

class SearchPresenter @Inject constructor(private val searchDataSource: SearchDataSource) : SearchContract.Presenter {
    private var view: SearchContract.View? = null

    override fun attach(view: SearchContract.View) {
        this.view = view
    }

    override fun submitSearch(query: String) {
        searchDataSource.multiSearch(query, object : DataSourceInterface<List<Media>>() {
            override fun onSuccess(mediaList: List<Media>) {
                view?.showSearchResult(mediaList)
            }
        })
    }

    override fun onSearchSuggestions(query: String?) {
        searchDataSource.multiSearch(query, object : DataSourceInterface<List<Media>>() {
            override fun onSuccess(mediaList: List<Media>) {
                view?.showSearchSuggestions(mediaList)
            }
        })
    }

    override fun onListItemClicked(media: Media, transitionViews: Array<View>) {
        if (media.isMovie) view?.goToMovieDetailActivityWithTransition(media, transitionViews) else view?.goToTvDetailActivityWithTransition(media, transitionViews)
    }

    override fun onSuggestionClicked(media: Media) {
        if (media.isMovie) view?.goToMovieDetailActivity(media) else view?.goToTvDetailActivity(media)
    }

}