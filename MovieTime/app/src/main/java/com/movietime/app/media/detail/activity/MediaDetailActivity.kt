package com.movietime.app.media.detail.activity

import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.transition.Transition
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityOptionsCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.movietime.app.R
import com.movietime.app.databinding.MediaDetailBinding
import com.movietime.app.media.detail.adapter.StaffAdapter
// import com.movietime.network.media.detail.adapter.TrailerAdapter
import com.movietime.app.media.detail.presenter.MediaDetailContract
// import com.movietime.network.media.detail.video.VideoFragment
import com.movietime.app.media.home.adapter.MediaAdapter
import com.movietime.app.media.model.Cast
import com.movietime.app.media.model.Crew
import com.movietime.app.media.model.Genre
import com.movietime.app.media.model.Media
import com.movietime.app.media.model.Video
import com.movietime.app.view.flowText.FlowTextHelper
import com.movietime.app.view.recyclerViewDecoration.HorizontalSpaceItemDecoration
import org.parceler.Parcels
import androidx.core.util.Pair

abstract class MediaDetailActivity : AppCompatActivity(), MediaDetailContract.View {

    protected lateinit var binding: MediaDetailBinding

    protected abstract fun initializePresenter(): MediaDetailContract.Presenter?
    private val media: Media by lazy { Parcels.unwrap<Media>(intent.getParcelableExtra(MEDIA_KEY)) }
    private var presenter: MediaDetailContract.Presenter? = null
    private var enterTransitionListener: Transition.TransitionListener? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MediaDetailBinding.inflate(LayoutInflater.from(this))
        initializeTransitionNames(media)
        initializeTransitionViews()
        presenter = initializePresenter()
        presenter?.start(media.id)
        //the first time the onCreate is called there is a transition
        if (savedInstanceState == null) presenter!!.onStartTransition()
        //postpone the enter transition till glide load the image
        supportPostponeEnterTransition()
        //add listener to know when the transition end
        addTransitionListener()
        initializeActionBar()
        initializeRecyclerView()
    }

    private fun initializeTransitionNames(media: Media?) {
        val resources = resources
        binding.posterImage.transitionName = resources.getString(R.string.poster_transition) + media!!.id
        binding.posterGradient.transitionName = resources.getString(R.string.gradient_poster_transition) + media.id
        binding.rate.transitionName = resources.getString(R.string.rate_transition) + media.id
        binding.cardView.transitionName = resources.getString(R.string.card_transition) + media.id
    }

    private fun initializeTransitionViews() { //when transition begin I try to load image from cache
        loadPosterImage(true)
        binding.rate.text = media.rating
        Glide.with(this)
                .load(media.getBackdropUrl())
                .into(binding.backdropImage)
    }

    private fun initializeActionBar() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = media.name
    }

    private fun initializeRecyclerView() {
        val space = Math.round(TypedValue
                .applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5f, resources.displayMetrics))
        val castLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(baseContext, LinearLayoutManager.HORIZONTAL, false)
        binding.cast.addItemDecoration(HorizontalSpaceItemDecoration(space))
        binding.cast.layoutManager = castLayoutManager
        val crewLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(baseContext, LinearLayoutManager.HORIZONTAL, false)
        binding.crew.addItemDecoration(HorizontalSpaceItemDecoration(space))
        binding.crew.layoutManager = crewLayoutManager
        val trailerLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(baseContext, LinearLayoutManager.HORIZONTAL, false)
        binding.trailerList.addItemDecoration(HorizontalSpaceItemDecoration(space))
        binding.trailerList.layoutManager = trailerLayoutManager
        val recommendationsLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(baseContext, LinearLayoutManager.HORIZONTAL, false)
        binding.recommendations.addItemDecoration(HorizontalSpaceItemDecoration(space))
        binding.recommendations.layoutManager = recommendationsLayoutManager
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finishWithAnimation()
            return true
        }
        return false
    }

    override fun onBackPressed() {
        finishWithAnimation()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.onDestroy()
        //to avoid memory leak
        // val trailerAdapter = binding.trailerList.adapter as TrailerAdapter?
        // trailerAdapter?.releaseLoaders()
    }

    private fun finishWithAnimation() { // this release the youtube player which allow to overlay the fragment
        // val videoFragment = supportFragmentManager.findFragmentById(R.id.videoFragment) as VideoFragment?
        // videoFragment?.finish()
        finishAfterTransition()
    }

    override fun showVideoList(videos: List<Video>) {
        // val videoFragment = VideoFragment.newInstance()
        //use commitAllowingStateLoss to avoid java.lang.IllegalStateException: Can not perform this action after onSaveInstanceState
//see https://stackoverflow.com/questions/7575921/illegalstateexception-can-not-perform-this-action-after-onsaveinstancestate-wit
//         supportFragmentManager.beginTransaction().replace(R.id.videoFragment, videoFragment as Fragment)
//                 .commitAllowingStateLoss()
//         videoFragment.setVideoId(videos[0].key)
        // binding.trailerList.adapter = TrailerAdapter(videos) { video -> presenter?.onVideoChose(video) }
        appearAnimation(binding.trailerTitle)
        appearAnimation(binding.trailerList)
    }

    override fun showYoutubeVideo(video: Video) {
        // val videoFragment = supportFragmentManager.findFragmentById(R.id.videoFragment) as VideoFragment?
        // videoFragment?.setVideoId(video.key)
    }

    override fun showCast(cast: List<Cast>) {
        binding.cast.adapter = StaffAdapter(cast, this)
        appearAnimation(binding.castTitle)
        appearAnimation(binding.cast)
    }

    override fun showCrew(crew: List<Crew>) {
        binding.crew.adapter = StaffAdapter(crew, this)
        appearAnimation(binding.crewTitle)
        appearAnimation(binding.crew)
    }

    override fun showRecommendations(recommendations: List<Media>) {
        binding.recommendations.adapter = MediaAdapter(recommendations, this, { media, transitionViews -> presenter?.onRecommendationClicked(media, transitionViews) })
        appearAnimation(binding.recommendationsTitle)
    }

    protected fun startDetailActivity(media: Media, transitionViews: Array<View>, intent: Intent) {
        intent.putExtra(MEDIA_KEY, Parcels.wrap(media))
        val pairList: Array<Pair<View, String>> = transitionViews.map { transitionView -> Pair(transitionView, transitionView.transitionName) }.toTypedArray()
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this@MediaDetailActivity, *pairList)
        startActivity(intent, options.toBundle())
    }

    override fun getMediaContext(): Context? {
        return baseContext
    }

    override fun showGenres(genres: List<Genre>) {
        appearAnimation(binding.genreContainer)
        for (genre in genres) {
            val tvGenre = LayoutInflater.from(this)
                    .inflate(R.layout.genre_text, binding.genreContainer, false) as TextView
            tvGenre.text = genre.name
            binding.genreContainer.addView(tvGenre)
        }
    }

    private fun initializeOverview() {
        initializeWrappingText()
        appearAnimation(binding.overview)
        appearAnimation(binding.overviewTitle)
    }

    protected fun appearAnimation(view: View?) {
        view?.visibility = View.VISIBLE
        view?.alpha = 0f
        view?.animate()?.withLayer()?.alpha(1f)?.start()
    }

    private fun initializeWrappingText() {
        val titleLayoutParams = binding.overviewTitle.layoutParams as ConstraintLayout.LayoutParams
        val overviewLayoutParams = binding.overview.layoutParams as ConstraintLayout.LayoutParams
        val offset = (titleLayoutParams.topMargin + binding.overviewTitle.height
                + overviewLayoutParams.topMargin)
        val display = windowManager.defaultDisplay
        FlowTextHelper.flowText(media.overview, binding.posterImage, binding.overview, display, offset)
    }

    private fun loadPosterImage(onlyRetrieveFromCache: Boolean) {
        val startTransitionAfterLoadImageListener: RequestListener<Drawable> = object : RequestListener<Drawable> {
            override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                supportStartPostponedEnterTransition()
                return false
            }

            override fun onResourceReady(resource: Drawable, model: Any, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                supportStartPostponedEnterTransition()
                return false
            }
        }
        Glide.with(this)
                .load(media.getPosterUrl())
                .listener(startTransitionAfterLoadImageListener)
                .apply(RequestOptions().dontTransform()
                        .onlyRetrieveFromCache(onlyRetrieveFromCache))
                .into(binding.posterImage)
    }

    private fun addTransitionListener() {
        enterTransitionListener = object : Transition.TransitionListener {
            override fun onTransitionStart(transition: Transition) {}
            override fun onTransitionEnd(transition: Transition) {
                initializeOverview()
                presenter?.onFinishTransition()
                //if transition end and image wasn't loaded, I fetch it from internet
                if (binding.posterImage.drawable == null) loadPosterImage(false)
                window.enterTransition.removeListener(enterTransitionListener)
            }

            override fun onTransitionCancel(transition: Transition) {}
            override fun onTransitionPause(transition: Transition) {}
            override fun onTransitionResume(transition: Transition) {}
        }
        window.enterTransition.addListener(enterTransitionListener)
    }

    companion object {
        const val MEDIA_KEY = "media_key"
    }
}