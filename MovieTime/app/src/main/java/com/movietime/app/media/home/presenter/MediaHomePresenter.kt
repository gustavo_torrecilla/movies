package com.movietime.app.media.home.presenter

import com.movietime.app.datasource.DataSourceInterface
import com.movietime.app.datasource.MediaDataSource
import com.movietime.app.media.model.Media

abstract class MediaHomePresenter protected constructor(private val mediaDataSource: MediaDataSource):
        MediaHomeContract.Presenter {
    protected var topRatedList: List<Media>? = null
    protected var popularList: List<Media>? = null
    protected var extraList: List<Media>? = null
    protected abstract fun getView(): MediaHomeContract.View?
    protected abstract fun obtainExtraList()
    override fun start() {
        if (getView() == null) return
        obtainPopular()
        obtainTopRated()
        obtainExtraList()
    }

    private fun obtainPopular() {
        mediaDataSource.popularList(getView()?.getMediaContext(), object : DataSourceInterface<List<Media>>() {
            override fun onSuccess(mediaList: List<Media>) {
                popularList = mediaList
                if (getView() != null) getView()?.showPopularMediaList(mediaList)
            }
        })
    }

    private fun obtainTopRated() {
        mediaDataSource.topRatedList(getView()?.getMediaContext(), object : DataSourceInterface<List<Media>>() {
            override fun onSuccess(mediaList: List<Media>) {
                topRatedList = mediaList
                if (getView() != null) getView()?.showTopRatedMediaList(mediaList)
            }
        })
    }

}