package com.movietime.app.media.detail

class TransitionRunningState(stateObserver: StateObserver) :
    TransitionState(stateObserver) {
    override fun onFinishLoading() {
        //do nothing on purpose
    }
}