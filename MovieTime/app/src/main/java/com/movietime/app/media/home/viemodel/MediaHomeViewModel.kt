package com.movietime.app.media.home.viemodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.movietime.app.media.model.Media

class MediaHomeViewModel(): ViewModel() {
    val _topRatedList: MutableLiveData<List<Media>> = MutableLiveData()
    val _popularList: MutableLiveData<List<Media>> = MutableLiveData()
    val _extraList: MutableLiveData<List<Media>> = MutableLiveData()
    val topRatedList: LiveData<List<Media>> = _topRatedList
    val popularList: LiveData<List<Media>> = _popularList
    val extraList: LiveData<List<Media>> = _extraList



}