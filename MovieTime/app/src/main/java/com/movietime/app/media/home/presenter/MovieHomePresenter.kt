package com.movietime.app.media.home.presenter

import android.view.View
import com.movietime.app.R
import com.movietime.app.datasource.DataSourceInterface
import com.movietime.app.datasource.MovieDataSource
import com.movietime.app.media.model.Media
import com.movietime.app.media.model.Movie
import javax.inject.Inject

class MovieHomePresenter @Inject constructor(private val movieDataSource: MovieDataSource):
        MediaHomePresenter(movieDataSource), MediaHomeContract.MoviePresenter {
    private var view: MediaHomeContract.MovieView? = null
    override fun attach(view: MediaHomeContract.MovieView) {
        this.view = view
    }

    override fun onListItemClicked(media: Media, transitionViews: Array<View>) {
        view?.goToMovieDetailActivity(media, transitionViews)
    }

    override fun seeAllTopRatedClicked() {
        topRatedList?.let {
            view?.goToMovieListActivity(it, view?.getMediaContext()?.getString(R.string.top_rated))
        }
    }

    override fun seeAllPopularClicked() {
        popularList?.let {
            view?.goToMovieListActivity(it, view?.getMediaContext()?.getString(R.string.popular))
        }
    }

    override fun seeAllExtraListClicked() {
        extraList?.let {
            view?.goToMovieListActivity(it, view?.getMediaContext()?.getString(R.string.upcoming))
        }
    }

    override fun onDestroy() {
        view = null
    }

    override fun obtainExtraList() {
        movieDataSource.upcomingList(view?.getMediaContext(), object : DataSourceInterface<List<Movie>>() {
            override fun onSuccess(movieList: List<Movie>) {
                extraList = movieList
                if (view != null) view?.showExtraMediaList(movieList)
            }
        })
    }

    public override fun getView(): MediaHomeContract.MovieView? {
        return view
    }

}