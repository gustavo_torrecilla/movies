package com.movietime.app.media.model

import com.google.gson.annotations.SerializedName

class Credits {
    @SerializedName("cast")
    var cast: List<Cast>? = null

    @SerializedName("crew")
    var crew: List<Crew>? = null

    @SerializedName("id")
    var id: Int? = null
}