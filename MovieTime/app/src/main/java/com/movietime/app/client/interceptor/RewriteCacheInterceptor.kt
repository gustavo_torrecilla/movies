package com.movietime.app.client.interceptor

import android.content.Context
import com.movietime.app.utils.NetworkUtil.hasInternetConnection
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class RewriteCacheInterceptor(var context: Context) : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalResponse = chain.proceed(chain.request())
        return if (!hasInternetConnection(context)) {
            val maxStale = 60 * 60 * 24 * 28 // tolerate 4-weeks stale
            originalResponse.newBuilder()
                .header("Cache-Control", "public, only-if-cached, max-stale=$maxStale")
                .build()
        } else originalResponse
    }
}