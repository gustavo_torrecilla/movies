package com.movietime.app.datasource

import android.content.Context
import com.movietime.app.media.model.Credits
import com.movietime.app.media.model.Genres
import com.movietime.app.media.model.Video

interface MediaDataSource {
    fun topRatedList(
        context: Context?,
        dataSourceInterface: DataSourceInterface<*>?
    )

    fun popularList(
        context: Context?,
        dataSourceInterface: DataSourceInterface<*>?
    )

    fun getVideos(
        mediaId: String?,
        context: Context?,
        dataSourceInterface: DataSourceInterface<List<Video>>
    )

    fun getCredits(
        mediaId: String?,
        context: Context?,
        dataSourceInterface: DataSourceInterface<Credits>
    )

    fun recommendations(
        mediaId: String?,
        context: Context?,
        dataSourceInterface: DataSourceInterface<*>
    )

    fun getGenres(
        context: Context?,
        dataSourceInterface: DataSourceInterface<Genres>
    )
}