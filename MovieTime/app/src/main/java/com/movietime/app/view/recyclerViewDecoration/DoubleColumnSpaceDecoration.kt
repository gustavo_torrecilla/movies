package com.movietime.app.view.recyclerViewDecoration

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class DoubleColumnSpaceDecoration(private val space: Int) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect, view: View, parent: RecyclerView,
        state: RecyclerView.State
    ) {
        outRect.bottom = space
        val position = parent.getChildAdapterPosition(view)
        if (position == 0 || position == 1) {
            outRect.top = space
        }
        if (position % 2 == 1) {
            outRect.right = space
            outRect.left = space / 2
        } else {
            outRect.right = space / 2
            outRect.left = space
        }
    }
}