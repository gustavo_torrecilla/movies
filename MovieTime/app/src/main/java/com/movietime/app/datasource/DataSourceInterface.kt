package com.movietime.app.datasource

abstract class DataSourceInterface<T> {
    abstract fun onSuccess(`object`: T)
    fun onInternalError() {}
    fun requestError() {}
    fun nullResponse() {}
}