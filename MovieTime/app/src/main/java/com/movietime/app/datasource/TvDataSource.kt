package com.movietime.app.datasource

import android.content.Context
import com.movietime.app.client.ServiceGenerator
import com.movietime.app.client.services.TvService
import com.movietime.app.media.model.Credits
import com.movietime.app.media.model.Genres
import com.movietime.app.media.model.Tv
import com.movietime.app.media.model.TvDetail
import com.movietime.app.media.model.Video
import javax.inject.Inject

class TvDataSource @Inject constructor() : MediaDataSource {
    fun onAirList(
        context: Context?,
        dataSourceInterface: DataSourceInterface<List<Tv>>
    ) {
        ServiceGenerator.createServiceWithCache(
            TvService::class.java,
            context
        )
            .onAirTvShows()
            .enqueue(DataSourceCallback(dataSourceInterface))
    }

    override fun topRatedList(
        context: Context?,
        dataSourceInterface: DataSourceInterface<*>?
    ) {
        ServiceGenerator.createServiceWithCache(
            TvService::class.java,
            context
        )
            .topRatedTvShows()
            .enqueue(DataSourceCallback<List<Tv>>(dataSourceInterface as DataSourceInterface<List<Tv>>))
    }

    override fun popularList(
        context: Context?,
        dataSourceInterface: DataSourceInterface<*>?
    ) {
        ServiceGenerator.createServiceWithCache(
            TvService::class.java,
            context
        )
            .popularTvShows()
            .enqueue(DataSourceCallback<List<Tv>>(dataSourceInterface as DataSourceInterface<List<Tv>>))
    }

    override fun getVideos(
        mediaId: String?,
        context: Context?,
        dataSourceInterface: DataSourceInterface<List<Video>>
    ) {
        ServiceGenerator.createServiceWithCache(
            TvService::class.java,
            context
        )
            .tvVideos(mediaId)
            .enqueue(
                DataSourceCallback(
                    dataSourceInterface
                )
            )
    }

    override fun getCredits(
        mediaId: String?,
        context: Context?,
        dataSourceInterface: DataSourceInterface<Credits>
    ) {
        ServiceGenerator.createServiceWithCache(
            TvService::class.java,
            context
        )
            .tvCredits(mediaId)
            .enqueue(DataSourceCallback(dataSourceInterface))
    }

    override fun recommendations(
        mediaId: String?,
        context: Context?,
        dataSourceInterface: DataSourceInterface<*>
    ) {
        ServiceGenerator.createServiceWithCache(
            TvService::class.java,
            context
        )
            .tvRecommendations(mediaId)
            .enqueue(DataSourceCallback<List<Tv>>(dataSourceInterface as DataSourceInterface<List<Tv>>))
    }

    fun getDetails(
        mediaId: String?,
        context: Context?,
        dataSourceInterface: DataSourceInterface<TvDetail>
    ) {
        ServiceGenerator.createServiceWithCache(
            TvService::class.java,
            context
        )
            .tvDetails(mediaId)
            .enqueue(DataSourceCallback(dataSourceInterface))
    }

    override fun getGenres(
        context: Context?,
        dataSourceInterface: DataSourceInterface<Genres>
    ) {
        ServiceGenerator.createServiceWithCache(
            TvService::class.java,
            context
        )
            .genres()
            .enqueue(
                DataSourceCallback(
                    dataSourceInterface
                )
            )
    }
}