package com.movietime.app.media.detail.presenter

import android.view.View
import com.movietime.app.datasource.DataSourceInterface
import com.movietime.app.datasource.MovieDataSource
import com.movietime.app.media.model.Media
import com.movietime.app.media.model.MovieDetail
import javax.inject.Inject

class MovieDetailPresenter @Inject constructor(private val movieDataSource: MovieDataSource) : MediaDetailPresenter(movieDataSource), MediaDetailContract.MoviePresenter {
    private var movieView: MediaDetailContract.MovieView? = null
    private var movieDetail: MovieDetail? = null
    override fun attach(movieView: MediaDetailContract.MovieView) {
        this.movieView = movieView
    }

    override fun onRecommendationClicked(media: Media, transitionViews: Array<View>) {
        movieView?.goToMovieDetailActivity(media, transitionViews)
    }

    override fun onDestroy() {
        movieView = null
    }

    override fun getView(): MediaDetailContract.View? {
        return movieView
    }

    override fun obtainDetails(mediaId: String) {
        movieDataSource.getDetails(mediaId, movieView?.getMediaContext(), object : DataSourceInterface<MovieDetail>() {
            override fun onSuccess(movieDetails: MovieDetail) {
                if (movieView == null) return
                movieDetail = movieDetails
                transitionState.onFinishLoading()
            }
        })
    }

    override fun detailsFinishLoading(): Boolean {
        return movieDetail != null
    }

    override fun showDetails() {
        movieDetail?.tagline?.let { if (!it.isEmpty()) movieView?.showTagLine(it) }
        movieDetail?.genres?.let { if (!it.isEmpty()) movieView?.showGenres(it) }
    }

}