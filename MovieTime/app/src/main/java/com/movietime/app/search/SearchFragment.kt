package com.movietime.app.search

import android.app.ActivityOptions
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityOptionsCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.arlib.floatingsearchview.FloatingSearchView.OnQueryChangeListener
import com.arlib.floatingsearchview.FloatingSearchView.OnSearchListener
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion
import com.bumptech.glide.Glide
import com.movietime.app.media.detail.activity.MediaDetailActivity
import com.movietime.app.media.detail.activity.MovieDetailActivity
import com.movietime.app.media.detail.activity.TvDetailActivity
import com.movietime.app.media.list.adapter.MediaListAdapter
import com.movietime.app.media.model.Media
import com.movietime.app.search.presenter.SearchContract
import com.movietime.app.view.recyclerViewDecoration.VerticalSpaceItemDecoration
import org.parceler.Parcels
import javax.inject.Inject
import androidx.core.util.Pair
import com.movietime.app.databinding.SearchFragmentBinding

class SearchFragment : Fragment(), SearchContract.View {

    @Inject
    lateinit var presenter: SearchContract.Presenter
    private var mediaListAdapter: MediaListAdapter? = null
    /**used to change suggestion after not writing for 500ms */
    private val suggestionHandler = Handler()
    private lateinit var binding: SearchFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = SearchFragmentBinding.inflate(inflater, container, false)
        presenter.attach(this)
        initializeSearchView()
        initializeRecyclerView()
        return binding.root
    }

    private fun initializeSearchView() {
        initializeSearchListener()
        initializeSuggestionQuery()
        initializeSuggestionBindCallback()
    }

    private fun initializeSearchListener() {
        binding.searchView.setOnSearchListener(object : OnSearchListener {
            override fun onSuggestionClicked(searchSuggestion: SearchSuggestion) {
                suggestionHandler.removeCallbacksAndMessages(null)
                presenter.onSuggestionClicked(searchSuggestion as Media)
            }

            override fun onSearchAction(currentQuery: String) {
                suggestionHandler.removeCallbacksAndMessages(null)
                presenter.submitSearch(currentQuery)
            }
        })
    }

    private fun initializeSuggestionQuery() {
        binding.searchView.setOnQueryChangeListener(object : OnQueryChangeListener {
            val QUERY_DELAY = 500
            val MIN_QUERY_LENGTH = 3
            var newQuery: String? = null
            var runnable = Runnable { presenter.onSearchSuggestions(newQuery) }

            override fun onSearchTextChanged(oldQuery: String, newQuery: String) {
                if (newQuery.length >= MIN_QUERY_LENGTH) {
                    this.newQuery = newQuery
                    suggestionHandler.removeCallbacksAndMessages(null)
                    suggestionHandler.postDelayed(runnable, QUERY_DELAY.toLong())
                }
            }
        })
    }

    private fun initializeSuggestionBindCallback() {
        binding.searchView.setOnBindSuggestionCallback { suggestionView, leftIcon, textView, item, itemPosition ->
            context?.let {
                Glide.with(it)
                        .load((item as Media).getPosterUrl())
                        .into(leftIcon)
            }
        }
    }

    private fun initializeRecyclerView() {
        val space = Math.round(TypedValue
                .applyDimension(TypedValue.COMPLEX_UNIT_DIP, 11f, resources.displayMetrics))
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvMediaList.addItemDecoration(VerticalSpaceItemDecoration(space))
        binding.rvMediaList.layoutManager = layoutManager
        mediaListAdapter = MediaListAdapter(context) { media, transitionViews -> presenter.onListItemClicked(media, transitionViews) }
        binding.rvMediaList.adapter = mediaListAdapter
    }

    override fun showSearchResult(searchResult: List<Media>) {
        mediaListAdapter?.setMediaList(searchResult)
        mediaListAdapter?.notifyDataSetChanged()
    }

    override fun showSearchSuggestions(searchSuggestions: List<Media>) {
        binding.searchView.swapSuggestions(searchSuggestions)
    }

    override fun goToMovieDetailActivityWithTransition(media: Media, transitionViews: Array<View>) {
        val intent = Intent(context, MovieDetailActivity::class.java)
        startDetailActivityWithTransition(media, transitionViews, intent)
    }

    override fun goToTvDetailActivityWithTransition(media: Media, transitionViews: Array<View>) {
        val intent = Intent(context, TvDetailActivity::class.java)
        startDetailActivityWithTransition(media, transitionViews, intent)
    }

    private fun startDetailActivityWithTransition(media: Media, transitionViews: Array<View>, intent: Intent) {
        intent.putExtra(MediaDetailActivity.MEDIA_KEY, Parcels.wrap(media))
        val pairList: Array<Pair<View, String>> = arrayOf()
        for (i in transitionViews.indices) {
            pairList[i] = Pair(transitionViews[i], transitionViews[i].transitionName)
        }
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity!!, *pairList)
        startActivity(intent, options.toBundle())
    }

    override fun goToMovieDetailActivity(media: Media) {
        val intent = Intent(context, MovieDetailActivity::class.java)
        intent.putExtra(MediaDetailActivity.MEDIA_KEY, Parcels.wrap(media))
        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(activity).toBundle())
    }

    override fun goToTvDetailActivity(media: Media) {
        val intent = Intent(context, TvDetailActivity::class.java)
        intent.putExtra(MediaDetailActivity.MEDIA_KEY, Parcels.wrap(media))
        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(activity).toBundle())
    }

    companion object {
        fun newInstance(): SearchFragment {
            return SearchFragment()
        }
    }
}