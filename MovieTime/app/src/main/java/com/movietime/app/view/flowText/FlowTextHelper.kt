package com.movietime.app.view.flowText

import android.text.SpannableString
import android.text.Spanned
import android.view.Display
import android.view.View
import android.widget.TextView

object FlowTextHelper {
    fun flowText(
        text: String?,
        thumbnailView: View,
        messageView: TextView,
        display: Display?,
        offset: Int
    ) {
        // Get height and width of the image and height of the text line
        val height = thumbnailView.height
        val width = thumbnailView.width
        val textLineHeight = messageView.paint.fontSpacing

        // Set the span according to the number of lines and width of the image
        val lines = Math.ceil((height - offset) / textLineHeight.toDouble()).toInt()
        val ss = SpannableString(text)
        ss.setSpan(
            MyLeadingMarginSpan2(lines, width),
            0,
            ss.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        messageView.text = ss
    }
}