package com.movietime.app.injection

import com.movietime.app.search.presenter.SearchContract
import com.movietime.app.search.presenter.SearchPresenter
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@Module
@InstallIn(ActivityComponent::class)
abstract class SearchModule {

    @Binds
    abstract fun providePresenter(searchPresenter: SearchPresenter): SearchContract.Presenter

}