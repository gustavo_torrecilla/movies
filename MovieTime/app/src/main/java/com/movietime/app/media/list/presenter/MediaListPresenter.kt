package com.movietime.app.media.list.presenter

import com.movietime.app.datasource.DataSourceInterface
import com.movietime.app.datasource.MediaDataSource
import com.movietime.app.media.list.model.AllGenresFilter
import com.movietime.app.media.list.model.ConcreteGenreFilter
import com.movietime.app.media.list.model.GenreFilter
import com.movietime.app.media.model.Genres
import java.util.ArrayList

abstract class MediaListPresenter protected constructor(private val mediaDataSource: MediaDataSource) :
    MediaListContract.Presenter {
    protected abstract fun getView(): MediaListContract.View?
    override fun start() {
        mediaDataSource.getGenres(
            getView()!!.getContext(),
            object : DataSourceInterface<Genres>() {
                override fun onSuccess(genres: Genres) {
                    if (getView() == null) return
                    val genreFilters: MutableList<GenreFilter> =
                        ArrayList()
                    for (genre in genres.genres) {
                        genreFilters.add(ConcreteGenreFilter(genre!!))
                    }
                    genreFilters.add(0, AllGenresFilter())
                    getView()!!.showGenresFilter(genreFilters)
                }
            })
    }
}