package com.movietime.app.datasource

import android.content.Context
import com.movietime.app.client.ServiceGenerator
import com.movietime.app.client.services.MovieService
import com.movietime.app.media.model.Credits
import com.movietime.app.media.model.Genres
import com.movietime.app.media.model.Movie
import com.movietime.app.media.model.MovieDetail
import com.movietime.app.media.model.Video
import javax.inject.Inject

class MovieDataSource @Inject constructor() : MediaDataSource {
    fun upcomingList(
        context: Context?,
        dataSourceInterface: DataSourceInterface<List<Movie>>
    ) {
        ServiceGenerator.createServiceWithCache(
            MovieService::class.java, context
        )
            .upcomingMovies()
            .enqueue(DataSourceCallback(dataSourceInterface))
    }

    override fun topRatedList(
        context: Context?,
        dataSourceInterface: DataSourceInterface<*>?
    ) {
        ServiceGenerator.createServiceWithCache(
            MovieService::class.java, context
        )
            .topRatedMovies()
            .enqueue(
                DataSourceCallback(
                    dataSourceInterface as DataSourceInterface<List<Movie>>
                )
            )
    }

    override fun popularList(
        context: Context?,
        dataSourceInterface: DataSourceInterface<*>?
    ) {
        ServiceGenerator.createServiceWithCache(
            MovieService::class.java, context
        )
            .popularMovies()
            .enqueue(
                DataSourceCallback<List<Movie>>(
                    dataSourceInterface as DataSourceInterface<List<Movie>>
                )
            )
    }

    override fun getVideos(
        mediaId: String?,
        context: Context?,
        dataSourceInterface: DataSourceInterface<List<Video>>
    ) {
        ServiceGenerator.createServiceWithCache(
            MovieService::class.java, context
        )
            .movieVideos(mediaId)
            .enqueue(
                DataSourceCallback(
                    dataSourceInterface
                )
            )
    }

    override fun getCredits(
        mediaId: String?,
        context: Context?,
        dataSourceInterface: DataSourceInterface<Credits>
    ) {
        ServiceGenerator.createServiceWithCache(
            MovieService::class.java, context
        )
            .movieCredits(mediaId)
            .enqueue(DataSourceCallback(dataSourceInterface))
    }

    override fun recommendations(
        mediaId: String?,
        context: Context?,
        dataSourceInterface: DataSourceInterface<*>
    ) {
        ServiceGenerator.createServiceWithCache(
            MovieService::class.java, context
        )
            .movieRecommendations(mediaId)
            .enqueue(
                DataSourceCallback<List<Movie>>(
                    dataSourceInterface as DataSourceInterface<List<Movie>>
                )
            )
    }

    fun getDetails(
        mediaId: String?,
        context: Context?,
        dataSourceInterface: DataSourceInterface<MovieDetail>
    ) {
        ServiceGenerator.createServiceWithCache(
            MovieService::class.java, context
        )
            .movieDetails(mediaId)
            .enqueue(DataSourceCallback(dataSourceInterface))
    }

    override fun getGenres(
        context: Context?,
        dataSourceInterface: DataSourceInterface<Genres>
    ) {
        ServiceGenerator.createServiceWithCache(
            MovieService::class.java, context
        )
            .genres()
            .enqueue(
                DataSourceCallback(
                    dataSourceInterface
                )
            )
    }
}