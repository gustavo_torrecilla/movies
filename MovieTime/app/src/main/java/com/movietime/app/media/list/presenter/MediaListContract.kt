package com.movietime.app.media.list.presenter

import android.content.Context
import com.movietime.app.media.list.model.GenreFilter
import com.movietime.app.media.model.Media

interface MediaListContract {
    interface View {
        fun showGenresFilter(genres: List<GenreFilter>)
        fun getContext(): Context
    }

    interface MovieView : View {
        fun goToMovieDetailActivity(
            media: Media,
            transitionViews: Array<android.view.View>
        )
    }

    interface TvView : View {
        fun goToTvDetailActivity(
            media: Media,
            transitionViews: Array<android.view.View>
        )
    }

    interface Presenter {
        fun start()
        fun onListItemClicked(
            media: Media,
            transitionViews: Array<android.view.View>
        )

        fun onDestroy()
    }

    interface MoviePresenter :
        Presenter {
        fun attach(view: MovieView?)
    }

    interface TvPresenter :
        Presenter {
        fun attach(view: TvView?)
    }
}