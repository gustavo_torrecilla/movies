package com.movietime.app.media.home.fragment

import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityOptionsCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.OnClick
import com.movietime.app.R
import com.movietime.app.databinding.MediaFragmentBinding
import com.movietime.app.media.detail.activity.MediaDetailActivity
import com.movietime.app.media.home.adapter.MediaAdapter
import com.movietime.app.media.home.adapter.MediaSliderAdapter
import com.movietime.app.media.home.presenter.MediaHomeContract
import com.movietime.app.media.list.activity.MediaListActivity
import com.movietime.app.media.model.Media
import com.movietime.app.view.pagerTransformer.DepthPageTransformer
import com.movietime.app.view.recyclerViewDecoration.HorizontalSpaceItemDecoration
import org.parceler.Parcels
import java.util.Timer
import java.util.TimerTask
import androidx.core.util.Pair


abstract class MediaHomeFragment : Fragment(), MediaHomeContract.View {

    protected lateinit var binding: MediaFragmentBinding
    private var timer: Timer? = null
    private var handler: Handler? = null
    private var pagerRunnable: Runnable? = null
    private val onItemClickListener = { media: Media, transitionViews: Array<View> -> presenter.onListItemClicked(media, transitionViews) }

    protected abstract fun setExtraListTitle()
    protected abstract fun initializeMediaHomePresenter()
    protected abstract val presenter: MediaHomeContract.Presenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = MediaFragmentBinding.inflate(inflater, container, false)
        initializeMediaHomePresenter()
        setExtraListTitle()
        initializeRecyclerView()
        initializeViewPager()
        presenter.start()
        initializeListeners()
        return binding.root
    }

    private fun initializeListeners() {
        binding.seeAllTopRated.setOnClickListener { presenter.seeAllTopRatedClicked() }
        binding.seeAllExtraList.setOnClickListener { presenter.seeAllExtraListClicked() }
        binding.seeAllPopular.setOnClickListener { presenter.seeAllPopularClicked() }
    }

    override fun getMediaContext(): Context? {
        return context
    }

    private fun initializeRecyclerView() {
        val space = Math.round(TypedValue
                .applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5f, resources.displayMetrics))
        val topRatedLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.rvTopRatedMedia.addItemDecoration(HorizontalSpaceItemDecoration(space))
        binding.rvTopRatedMedia.layoutManager = topRatedLayoutManager
        val upcomingLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.rvExtraListMedia.addItemDecoration(HorizontalSpaceItemDecoration(space))
        binding.rvExtraListMedia.layoutManager = upcomingLayoutManager
    }

    private fun initializeViewPager() {
        binding.vpPopularMedia.setPageTransformer(true, DepthPageTransformer())
        binding.vpPopularMedia.offscreenPageLimit = 2
        handler = Handler()
        pagerRunnable = Runnable {
            val pagerAdapter = binding.vpPopularMedia.adapter
            if (pagerAdapter != null &&
                    binding.vpPopularMedia.currentItem != pagerAdapter.count - 1) binding.vpPopularMedia.setCurrentItem(binding.vpPopularMedia.currentItem + 1, true)
        }
    }

    override fun onPause() {
        super.onPause()
        cancelAutomaticPagerScroll()
    }

    override fun onResume() {
        super.onResume()
        startAutomaticPagerScroll()
    }

    private fun startAutomaticPagerScroll() {
        timer = Timer()
        timer!!.schedule(object : TimerTask() {
            override fun run() {
                handler!!.post(pagerRunnable)
            }
        }, 4000, 4000)
    }

    private fun cancelAutomaticPagerScroll() {
        timer!!.cancel()
        timer!!.purge()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    @OnClick(R.id.seeAllTopRated)
    fun seeAllTopRated() {
        presenter.seeAllTopRatedClicked()
    }

    @OnClick(R.id.seeAllExtraList)
    fun seeAllExtraList() {
        presenter.seeAllExtraListClicked()
    }

    @OnClick(R.id.seeAllPopular)
    fun seeAllPopular() {
        presenter.seeAllPopularClicked()
    }

    //view contract
    override fun showPopularMediaList(mediaList: List<Media>) {
        binding.vpPopularMedia.adapter = MediaSliderAdapter(mediaList, context) { media: Media, transitionViews: Array<View> -> presenter.onListItemClicked(media, transitionViews) }
    }

    override fun showTopRatedMediaList(mediaList: List<Media>) {
        val adapter = MediaAdapter(mediaList, context, onItemClickListener)
        binding.rvTopRatedMedia.adapter = adapter
    }

    override fun showExtraMediaList(mediaList: List<Media>) {
        val adapter = MediaAdapter(mediaList, context, onItemClickListener)
        binding.rvExtraListMedia.adapter = adapter
    }

    protected fun startDetailActivity(intent: Intent, media: Media, transitionViews: Array<View>) {
        intent.putExtra(MediaDetailActivity.MEDIA_KEY, Parcels.wrap(media))
        val pairList: Array<Pair<View, String>> = transitionViews.map { transitionView -> Pair.create<View, String>(transitionView, transitionView.transitionName) }.toTypedArray()
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity!!, *pairList)
        startActivity(intent, options.toBundle())
    }

    protected fun startListActivity(intent: Intent, mediaList: List<Media>, title: String?) {
        intent.putExtra(MediaListActivity.LIST_KEY, Parcels.wrap(mediaList))
        intent.putExtra(MediaListActivity.TITLE_KEY, title)
        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(activity).toBundle())
    }
}