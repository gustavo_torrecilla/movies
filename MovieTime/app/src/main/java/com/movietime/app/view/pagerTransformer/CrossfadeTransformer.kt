package com.movietime.app.view.pagerTransformer

import android.view.View
import androidx.viewpager.widget.ViewPager

class CrossfadeTransformer : ViewPager.PageTransformer {
    override fun transformPage(
        page: View,
        position: Float
    ) {
        if (position >= -1 && position <= 1) {
            page.alpha = 1 - Math.abs(position)
        } else {
            page.alpha = 0f
        }
    }
}