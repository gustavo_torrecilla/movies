package com.movietime.app.datasource

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DataSourceCallback<T>(private val dataSourceInterface: DataSourceInterface<T>) :
    Callback<T> {
    override fun onResponse(
        call: Call<T>,
        response: Response<T>
    ) {
        if (response.isSuccessful) {
            val responseModel = response.body()
            if (responseModel != null) dataSourceInterface.onSuccess(responseModel) else dataSourceInterface.nullResponse()
        } else {
            dataSourceInterface.requestError()
        }
    }

    override fun onFailure(call: Call<T>, t: Throwable) {
        dataSourceInterface.onInternalError()
    }
}