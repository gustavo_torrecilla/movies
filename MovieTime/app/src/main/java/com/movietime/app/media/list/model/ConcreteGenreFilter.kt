package com.movietime.app.media.list.model

import com.movietime.app.media.model.Genre
import com.movietime.app.media.model.Media
import java.util.ArrayList

class ConcreteGenreFilter(private val genre: Genre) : GenreFilter() {
    override fun toString(): String {
        return genre.name
    }

    override fun filter(mediaList: List<Media>): List<Media> {
        val filteredList: MutableList<Media> =
            ArrayList()
        for (media in mediaList) {
            if (media.genreIds.contains(genre.id)) filteredList.add(media)
        }
        return filteredList
    }
}