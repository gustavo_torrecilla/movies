package com.movietime.app.media.home.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.movietime.app.R
import com.movietime.app.media.detail.activity.MovieDetailActivity
import com.movietime.app.media.home.presenter.MediaHomeContract
import com.movietime.app.media.list.activity.MovieListActivity
import com.movietime.app.media.model.Media
import javax.inject.Inject

class MovieHomeFragment : MediaHomeFragment(), MediaHomeContract.MovieView {

    @Inject
    override lateinit var presenter: MediaHomeContract.MoviePresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun setExtraListTitle() {
        binding.extraListTitle.setText(R.string.upcoming)
    }

    override fun initializeMediaHomePresenter() {
        presenter.attach(this)
    }

    override fun goToMovieListActivity(mediaList: List<Media>, title: String?) {
        val movieListIntent = Intent(context, MovieListActivity::class.java)
        startListActivity(movieListIntent, mediaList, title)
    }

    override fun goToMovieDetailActivity(media: Media, transitionViews: Array<View>) {
        val intent = Intent(context, MovieDetailActivity::class.java)
        startDetailActivity(intent, media, transitionViews)
    }

    companion object {
        fun newInstance(): MovieHomeFragment {
            return MovieHomeFragment()
        }
    }
}