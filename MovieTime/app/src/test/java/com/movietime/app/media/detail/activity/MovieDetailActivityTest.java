package com.movietime.app.media.detail.activity;

import android.content.Intent;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.movietime.app.R;
import com.movietime.app.TestApplication;
import com.movietime.app.media.detail.presenter.MediaDetailContract;
import com.movietime.app.media.model.Media;
import com.movietime.app.media.model.Movie;
import com.movietime.app.media.model.Video;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.parceler.Parcels;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import static com.movietime.app.media.detail.activity.MediaDetailActivity.MEDIA_KEY;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

/**This class test the interaction between the activity and the presenter.
 * It could also test that the activity show the correct data*/
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class MovieDetailActivityTest {

    @Inject
    MediaDetailContract.MoviePresenter presenter;
    private ActivityController<MovieDetailActivity> controller;

    @Before
    public void setUp(){
        TestApplication.getTestAppComponent().inject(this);
        Movie movie = new Movie("0", "url poster", "url backdrop", "7.4", "overview", new ArrayList<Integer>(), "prueba");
        Intent intent = new Intent().putExtra(MEDIA_KEY, Parcels.wrap(movie));
        controller = Robolectric.buildActivity(MovieDetailActivity.class, intent);
    }

    @Test
    public void onCreateViewIsAttachedOnPresenterAndPresenterIsStarted(){
        MovieDetailActivity activity = controller.create().get();
        verify(presenter).attach(activity);
        verify(presenter).start("0");
    }

    @Test
    public void presenterDestroyCalledOnActivityOnDestroy(){
        controller.create();
        controller.destroy();
        verify(presenter).onDestroy();
    }

    @Test
    public void whenRecommendationListItemClicked_PresenterMethodOnRecommendationClickedCalled(){
        MovieDetailActivity activity = controller.create().get();
        List<Media> recommendations = new ArrayList<>();
        recommendations.add(Mockito.mock(Movie.class));
        activity.showRecommendations(recommendations);

        performClickOnFirstItemOfList(activity, R.id.recommendations);

        verify(presenter).onRecommendationClicked(any(Media.class), any(View[].class));
    }

    @Test
    public void whenListItemClicked_PresenterMethodOnRecommendationClickedCalled(){
        MovieDetailActivity activity = controller.create().get();
        List<Video> videos = new ArrayList<>();
        Video video = new Video();
        videos.add(video);
        activity.showVideoList(videos);

        performClickOnFirstItemOfList(activity, R.id.trailerList);

        verify(presenter).onVideoChose(video);
    }

    private void performClickOnFirstItemOfList(MovieDetailActivity activity, int listId) {
        RecyclerView currentRecyclerView =  activity.findViewById(listId);
        currentRecyclerView.measure(0, 0);
        currentRecyclerView.layout(0, 0, 100, 10000);
        currentRecyclerView.findViewHolderForPosition(0).itemView.performClick();
    }

}
