package com.movietime.app.media.list.presenter;

import android.content.Context;

import com.movietime.app.datasource.DataSourceInterface;
import com.movietime.app.datasource.MediaDataSource;
import com.movietime.app.media.list.model.GenreFilter;
import com.movietime.app.media.model.Genre;
import com.movietime.app.media.model.Genres;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public abstract class MediaListPresenterTest {
    @Captor
    protected ArgumentCaptor<DataSourceInterface> datasourceInterface;

    protected abstract MediaListContract.Presenter getPresenter();
    protected abstract MediaListContract.View getView();
    protected abstract MediaDataSource getMediaDataSource();
    protected abstract void initializePresenter();

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        initializePresenter();
        getPresenter().start();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void whenGenresLoadedViewShowFilters(){
        genresLoaded();
        verify(getView()).showGenresFilter(anyListOf(GenreFilter.class));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void whenGenresLoadedAfterOnDestroyViewDontShowFilters(){
        getPresenter().onDestroy();
        genresLoaded();
        verify(getView(), never()).showGenresFilter(anyListOf(GenreFilter.class));
    }

    private void genresLoaded() {
        Genres genres = Mockito.mock(Genres.class);
        when(genres.genres).thenReturn(new ArrayList<Genre>());
        verify(getMediaDataSource()).getGenres(any(Context.class), datasourceInterface.capture());
        datasourceInterface.getValue().onSuccess(genres);
    }
}
