package com.movietime.app.injection;

import com.movietime.app.media.detail.activity.MovieDetailActivity;
import com.movietime.app.media.detail.presenter.MediaDetailContract;

import org.mockito.Mockito;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public abstract class TestMovieDetailModule {
    @Provides
    @Singleton
    static MediaDetailContract.MoviePresenter providePresenter(){
        return Mockito.mock(MediaDetailContract.MoviePresenter.class);
    }
}
