package com.movietime.app;

import com.movietime.app.injection.DaggerTestAppComponent;
import com.movietime.app.injection.TestAppComponent;

public class TestApplication extends MediaApplication {

    /**Making the component static is the way I found
     * to share a mocked presenter between the activity and the test.
     *
     * I made the presenter provide method in the module singleton.
     * This way, if I can access the same component from the test,
     * then I can obtain the same presenter that the activity holds.
     *
     * There is not much info about how to do this on the web*/
    private static TestAppComponent testAppComponent;
    @Override
    public void onCreate() {
        super.onCreate();
        testAppComponent = DaggerTestAppComponent.create();
                testAppComponent.inject(this);
    }

    public static TestAppComponent getTestAppComponent() {
        return testAppComponent;
    }
}
