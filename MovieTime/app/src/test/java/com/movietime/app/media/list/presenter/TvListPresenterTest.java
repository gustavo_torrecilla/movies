package com.movietime.app.media.list.presenter;

import android.view.View;

import com.movietime.app.datasource.MediaDataSource;
import com.movietime.app.datasource.TvDataSource;
import com.movietime.app.media.model.Media;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.mockito.Mockito.verify;

public class TvListPresenterTest extends MediaListPresenterTest {
    MediaListContract.TvPresenter tvPresenter;
    @Mock
    private MediaListContract.TvView view;
    @Mock
    private TvDataSource tvDataSource;

    @Override
    protected MediaListContract.Presenter getPresenter() {
        return tvPresenter;
    }

    @Override
    protected MediaListContract.View getView() {
        return view;
    }

    @Override
    protected MediaDataSource getMediaDataSource() {
        return tvDataSource;
    }

    @Override
    protected void initializePresenter() {
        tvPresenter = new TvListPresenter(tvDataSource);
        tvPresenter.attach(view);
    }

    @Test
    public void whenListItemIsClickedGoToTvDetail(){
        Media media = Mockito.mock(Media.class);
        View[] views = new View[1];
        tvPresenter.onListItemClicked(media, views);
        verify(view).goToTvDetailActivity(media, views);
    }
}
