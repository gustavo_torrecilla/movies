package com.movietime.app.media.home.presenter;

import android.content.Context;

import com.movietime.app.datasource.DataSourceInterface;
import com.movietime.app.datasource.MediaDataSource;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public abstract class MediaHomePresenterTest {
    @Captor
    protected ArgumentCaptor<DataSourceInterface> datasourceInterface;

    protected abstract MediaHomeContract.Presenter getPresenter();
    protected abstract MediaHomeContract.View getView();
    protected abstract MediaDataSource getMediaDataSource();
    protected abstract void verifyExtraListFetchedFromDataSource();
    protected abstract void verifyExtraListNotFetchedFromDataSource();
    protected abstract void initializePresenter();

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        initializePresenter();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void whenStartViewShowPopularList(){
        getPresenter().start();
        verify(getMediaDataSource()).popularList(any(Context.class), datasourceInterface.capture());
        datasourceInterface.getValue().onSuccess(any());
        verify(getView()).showPopularMediaList(any(List.class));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void whenStartViewShowTopRatedList(){
        getPresenter().start();
        verify(getMediaDataSource()).topRatedList(any(Context.class), datasourceInterface.capture());
        datasourceInterface.getValue().onSuccess(any());
        verify(getView()).showTopRatedMediaList(any(List.class));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void whenStartViewShowOnAirList(){
        getPresenter().start();
        verifyExtraListFetchedFromDataSource();
        datasourceInterface.getValue().onSuccess(any());
        verify(getView()).showExtraMediaList(any(List.class));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void whenStartIsCalledAfterOnDestroyNothingHappenOnView(){
        getPresenter().onDestroy();
        getPresenter().start();
        verify(getMediaDataSource(), never()).popularList(any(Context.class), datasourceInterface.capture());
        verify(getMediaDataSource(), never()).topRatedList(any(Context.class), datasourceInterface.capture());
        verifyExtraListNotFetchedFromDataSource();
    }
}
