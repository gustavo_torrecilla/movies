package com.movietime.app.injection;

import com.movietime.app.media.detail.activity.MovieDetailActivityTest;
import com.movietime.app.TestApplication;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {AndroidSupportInjectionModule.class,
        TvHomeModule.class,
        MovieHomeModule.class,
        TestMovieDetailModule.class,
        TvDetailModule.class,
        MovieListModule.class,
        TvListModule.class})
public interface TestAppComponent {
    void inject(TestApplication app);
    void inject(MovieDetailActivityTest test);
}