package com.movietime.app.media.detail.presenter;

import android.content.Context;
import android.view.View;

import com.movietime.app.datasource.MediaDataSource;
import com.movietime.app.datasource.MovieDataSource;
import com.movietime.app.media.model.Genre;
import com.movietime.app.media.model.Media;
import com.movietime.app.media.model.MovieDetail;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MovieDetailPresenterTest extends MediaDetailPresenterTest{
    private static final String SOME_COOL_TAGLINE = "Some cool tagline";
    @Mock
    private MediaDetailContract.MovieView movieView;
    @Mock
    private MovieDataSource movieDataSource;
    private MediaDetailContract.MoviePresenter moviePresenter;

    @Override
    protected MediaDataSource getMediaDataSource() {
        return movieDataSource;
    }

    @Override
    protected MediaDetailContract.Presenter getMediaPresenter() {
        return moviePresenter;
    }

    @Override
    protected MediaDetailContract.View getView() {
        return movieView;
    }

    @Override
    protected void verifyNeverShowDetails() {
        verify(movieView, never()).showTagLine(any(String.class));

    }

    @Override
    protected void verifyShowDetails() {
        verify(movieView).showTagLine(SOME_COOL_TAGLINE);
    }

    @Override
    protected void loadDetails() {
        List<Genre> genres = new ArrayList<Genre>();
        genres.add(Mockito.mock(Genre.class));
        MovieDetail movieDetail = Mockito.mock(MovieDetail.class);
        when(movieDetail.tagline).thenReturn(SOME_COOL_TAGLINE);
        when(movieDetail.genres).thenReturn(genres);
        verify(movieDataSource).getDetails(any(String.class), any(Context.class), datasourceInterface.capture());
        datasourceInterface.getValue().onSuccess(movieDetail);
    }

    @Override
    protected void loadDetailsEmpty() {
        MovieDetail movieDetail = Mockito.mock(MovieDetail.class);
        when(movieDetail.tagline).thenReturn("");
        when(movieDetail.genres).thenReturn(new ArrayList<Genre>());
        verify(movieDataSource).getDetails(any(String.class), any(Context.class), datasourceInterface.capture());
        datasourceInterface.getValue().onSuccess(movieDetail);
    }

    @Override
    protected void initializePresenter() {
        moviePresenter = new MovieDetailPresenter(movieDataSource);
        moviePresenter.attach(movieView);
    }


    @Test
    public void whenRecommendationItemClickedGoToMovieDetail(){
        Media media = Mockito.mock(Media.class);
        View[] views = new View[1];
        moviePresenter.onRecommendationClicked(media, views);
        verify(movieView).goToMovieDetailActivity(media, views);
    }

}
