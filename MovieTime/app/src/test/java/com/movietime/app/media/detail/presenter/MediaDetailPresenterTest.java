package com.movietime.app.media.detail.presenter;

import android.content.Context;

import com.movietime.app.datasource.DataSourceInterface;
import com.movietime.app.datasource.MediaDataSource;
import com.movietime.app.media.model.Cast;
import com.movietime.app.media.model.Credits;
import com.movietime.app.media.model.Crew;
import com.movietime.app.media.model.Genre;
import com.movietime.app.media.model.Media;
import com.movietime.app.media.model.Video;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public abstract class MediaDetailPresenterTest {
    @Mock
    private MediaDetailContract.View view;
    private MediaDetailContract.Presenter presenter;
    @Captor
    protected ArgumentCaptor<DataSourceInterface> datasourceInterface;
    protected static final List<Genre> GENRE_LIST = new ArrayList<>();

    protected abstract MediaDataSource getMediaDataSource();
    protected abstract MediaDetailContract.Presenter getMediaPresenter();
    protected abstract MediaDetailContract.View getView();
    protected abstract void initializePresenter();
    protected abstract void verifyNeverShowDetails();
    protected abstract void loadDetails();
    protected abstract void loadDetailsEmpty();
    protected abstract void verifyShowDetails();

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        initializePresenter();
        presenter = getMediaPresenter();
        view = getView();
        presenter.start("id");
        presenter.onStartTransition();
        GENRE_LIST.add(new Genre(1, "Drama"));
        GENRE_LIST.add(new Genre(2, "Action"));
    }

    @SuppressWarnings("unchecked")
    protected void allDataIsLoaded() {
        List<Video> videos = new ArrayList<>();
        videos.add(Mockito.mock(Video.class));
        List<Media> recommendations = new ArrayList<>();
        recommendations.add(Mockito.mock(Media.class));
        List<Cast> cast = new ArrayList<>();
        cast.add(Mockito.mock(Cast.class));
        List<Crew> crew = new ArrayList<>();
        crew.add(Mockito.mock(Crew.class));

        Credits credits = Mockito.mock(Credits.class);
        when(credits.cast).thenReturn(cast);
        when(credits.crew).thenReturn(crew);

        verify(getMediaDataSource()).getVideos(any(String.class), any(Context.class), datasourceInterface.capture());
        datasourceInterface.getValue().onSuccess(videos);
        verify(getMediaDataSource()).getCredits(any(String.class), any(Context.class), datasourceInterface.capture());
        datasourceInterface.getValue().onSuccess(credits);
        verify(getMediaDataSource()).recommendations(any(String.class), any(Context.class), datasourceInterface.capture());
        datasourceInterface.getValue().onSuccess(recommendations);
        loadDetails();
    }

    @SuppressWarnings("unchecked")
    protected void allDataIsLoadedEmpty() {
        Credits credits = Mockito.mock(Credits.class);
        when(credits.cast).thenReturn(new ArrayList<Cast>());
        when(credits.crew).thenReturn(new ArrayList<Crew>());

        verify(getMediaDataSource()).getVideos(any(String.class), any(Context.class), datasourceInterface.capture());
        datasourceInterface.getValue().onSuccess(new ArrayList<>());
        verify(getMediaDataSource()).getCredits(any(String.class), any(Context.class), datasourceInterface.capture());
        datasourceInterface.getValue().onSuccess(credits);
        verify(getMediaDataSource()).recommendations(any(String.class), any(Context.class), datasourceInterface.capture());
        datasourceInterface.getValue().onSuccess(new ArrayList<>());
        loadDetailsEmpty();
    }


    @Test
    @SuppressWarnings("unchecked")
    public void whenTransitionRunningViewDontShowNothing(){
        allDataIsLoaded();

        viewDontShowNothing();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void whenAllDataIsNotLoadedViewDontShowNothing(){
        presenter.onFinishTransition();
        verify(getMediaDataSource()).recommendations(any(String.class), any(Context.class), datasourceInterface.capture());
        datasourceInterface.getValue().onSuccess(new ArrayList<>());

        viewDontShowNothing();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void whenDataIsEmptyViewDontShowIt(){
        presenter.onFinishTransition();
        allDataIsLoadedEmpty();

        viewDontShowNothing();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void whenPresenterIsDestroyedViewDontShowIt(){
        presenter.onFinishTransition();
        presenter.onDestroy();
        allDataIsLoaded();

        viewDontShowNothing();
    }

    private void viewDontShowNothing() {
        verify(getView(), never()).showCast(anyList());
        verify(getView(), never()).showCrew(anyList());
        verify(getView(), never()).showRecommendations(anyList());
        verify(getView(), never()).showGenres(anyList());
        verify(getView(), never()).showVideoList(anyList());
        verifyNeverShowDetails();
    }


    @Test
    @SuppressWarnings("unchecked")
    public void whenTransitionEndAndAllDataLoaded_ViewShowData(){
        presenter.onFinishTransition();
        allDataIsLoaded();
        verify(view).showVideoList(anyList());
        verify(view).showGenres(anyList());
        verify(view).showRecommendations(anyList());
        verify(view).showCrew(anyList());
        verify(view).showCast(anyList());
        verifyShowDetails();
    }

    @Test
    public void whenVideoChoseShowVideo(){
        Video video = Mockito.mock(Video.class);
        presenter.onVideoChose(video);
        verify(view).showYoutubeVideo(video);
    }

}
