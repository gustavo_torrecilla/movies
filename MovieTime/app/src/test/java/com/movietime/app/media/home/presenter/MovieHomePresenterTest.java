package com.movietime.app.media.home.presenter;

import android.content.Context;

import com.movietime.app.datasource.MediaDataSource;
import com.movietime.app.datasource.MovieDataSource;

import org.mockito.Mock;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class MovieHomePresenterTest extends MediaHomePresenterTest{

    @Mock
    private MediaHomeContract.MovieView view;
    @Mock
    private MovieDataSource movieDataSource;
    private MediaHomeContract.MoviePresenter presenter;

    @Override
    protected void initializePresenter() {
        presenter = new MovieHomePresenter(movieDataSource);
        presenter.attach(view);
    }

    @Override
    protected MediaHomeContract.Presenter getPresenter() {
        return presenter;
    }

    @Override
    protected MediaHomeContract.View getView() {
        return view;
    }

    @Override
    protected MediaDataSource getMediaDataSource() {
        return movieDataSource;
    }

    @Override
    protected void verifyExtraListFetchedFromDataSource() {
        verify(movieDataSource).upcomingList(any(Context.class), datasourceInterface.capture());
    }

    @Override
    protected void verifyExtraListNotFetchedFromDataSource() {
        verify(movieDataSource, never()).upcomingList(any(Context.class), datasourceInterface.capture());
    }
}
