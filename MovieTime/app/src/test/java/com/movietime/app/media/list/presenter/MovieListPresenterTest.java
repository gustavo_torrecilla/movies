package com.movietime.app.media.list.presenter;

import android.view.View;

import com.movietime.app.datasource.MediaDataSource;
import com.movietime.app.datasource.MovieDataSource;
import com.movietime.app.media.model.Media;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.mockito.Mockito.verify;

public class MovieListPresenterTest extends MediaListPresenterTest {
    MediaListContract.MoviePresenter moviePresenter;
    @Mock
    private MediaListContract.MovieView view;
    @Mock
    private MovieDataSource movieDataSource;

    @Override
    protected MediaListContract.Presenter getPresenter() {
        return moviePresenter;
    }

    @Override
    protected MediaListContract.View getView() {
        return view;
    }

    @Override
    protected MediaDataSource getMediaDataSource() {
        return movieDataSource;
    }

    @Override
    protected void initializePresenter() {
        moviePresenter = new MovieListPresenter(movieDataSource);
        moviePresenter.attach(view);
    }


    @Test
    public void whenListItemIsClickedGoToMovieDetail(){
        Media media = Mockito.mock(Media.class);
        View[] views = new View[1];
        moviePresenter.onListItemClicked(media, views);
        verify(view).goToMovieDetailActivity(media, views);
    }
}
