package com.movietime.app.search;

import android.view.View;

import com.movietime.app.datasource.DataSourceInterface;
import com.movietime.app.datasource.SearchDataSource;
import com.movietime.app.media.model.Movie;
import com.movietime.app.media.model.Tv;
import com.movietime.app.search.presenter.SearchContract;
import com.movietime.app.search.presenter.SearchPresenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.verify;

public class SearchPresenterTest {
    private static final String QUERY = "query";
    @Mock
    private SearchContract.View view;
    @Mock
    private SearchDataSource searchDataSource;
    private SearchContract.Presenter presenter;
    @Captor
    private ArgumentCaptor<DataSourceInterface> datasourceInterface;

    private Movie MOVIE;
    private Tv TV;
    private View[] VIEWS;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        presenter = new SearchPresenter(searchDataSource);
        presenter.attach(view);

        MOVIE = new Movie("1",
                "poster_url",
                "backdrop_url",
                "8.0",
                "overview",
                new ArrayList<Integer>(),
                "Fight Club");
        TV = new Tv("1",
                "poster_url",
                "backdrop_url",
                "8.0",
                "overview",
                new ArrayList<Integer>(),
                "Game of Thrones");
        VIEWS = new View[1];
    }

    @Test
    @SuppressWarnings("unchecked")
    public void whenSubmitSearch_ViewShowSearchResult(){
        presenter.submitSearch(QUERY);
        verify(searchDataSource).multiSearch(any(String.class), datasourceInterface.capture());
        datasourceInterface.getValue().onSuccess(anyList());
        verify(view).showSearchResult(anyList());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void whenSearchSuggestion_ViewShowSearchSuggestions(){
        presenter.onSearchSuggestions(QUERY);
        verify(searchDataSource).multiSearch(any(String.class), datasourceInterface.capture());
        datasourceInterface.getValue().onSuccess(anyList());
        verify(view).showSearchSuggestions(anyList());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void whenListMovieItemClicked_ViewGoToMovieDetailActivity(){
        presenter.onListItemClicked(MOVIE, VIEWS);
        verify(view).goToMovieDetailActivityWithTransition(MOVIE, VIEWS);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void whenSuggestionMovieItemClicked_ViewGoToMovieDetailActivity(){
        presenter.onSuggestionClicked(MOVIE);
        verify(view).goToMovieDetailActivity(MOVIE);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void whenListTvItemClicked_ViewGoToMovieDetailActivity(){
        presenter.onListItemClicked(TV, VIEWS);
        verify(view).goToTvDetailActivityWithTransition(TV, VIEWS);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void whenSuggestionTvItemClicked_ViewGoToMovieDetailActivity(){
        presenter.onSuggestionClicked(TV);
        verify(view).goToTvDetailActivity(TV);
    }
}
