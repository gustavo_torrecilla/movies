package com.movietime.app.media.home.presenter;

import android.content.Context;

import com.movietime.app.datasource.MediaDataSource;
import com.movietime.app.datasource.TvDataSource;

import org.mockito.Mock;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class TvHomePresenterTest extends MediaHomePresenterTest{
    @Mock
    private MediaHomeContract.TvView view;
    @Mock
    private TvDataSource tvDataSource;
    private MediaHomeContract.TvPresenter presenter;

    @Override
    protected void initializePresenter() {
        presenter = new TvHomePresenter(tvDataSource);
        presenter.attach(view);
    }

    @Override
    protected MediaHomeContract.Presenter getPresenter() {
        return presenter;
    }

    @Override
    protected MediaHomeContract.View getView() {
        return view;
    }

    @Override
    protected MediaDataSource getMediaDataSource() {
        return tvDataSource;
    }

    @Override
    protected void verifyExtraListFetchedFromDataSource() {
        verify(tvDataSource).onAirList(any(Context.class), datasourceInterface.capture());
    }

    @Override
    protected void verifyExtraListNotFetchedFromDataSource() {
        verify(tvDataSource, never()).onAirList(any(Context.class), datasourceInterface.capture());
    }
}
