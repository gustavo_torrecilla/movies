package com.movietime.app.media.detail.presenter;

import android.content.Context;
import android.view.View;

import com.movietime.app.datasource.MediaDataSource;
import com.movietime.app.datasource.TvDataSource;
import com.movietime.app.media.model.Genre;
import com.movietime.app.media.model.Media;
import com.movietime.app.media.model.TvDetail;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TvDetailPresenterTest extends MediaDetailPresenterTest{
    @Mock
    private TvDataSource tvDataSource;
    private TvDetailPresenter tvDetailPresenter;
    @Mock
    private MediaDetailContract.TvView tvView;

    @Override
    protected MediaDataSource getMediaDataSource() {
        return tvDataSource;
    }

    @Override
    protected MediaDetailContract.Presenter getMediaPresenter() {
        return tvDetailPresenter;
    }

    @Override
    protected MediaDetailContract.View getView() {
        return tvView;
    }

    @Override
    protected void initializePresenter() {
        tvDetailPresenter = new TvDetailPresenter(tvDataSource);
        tvDetailPresenter.attach(tvView);
    }

    @Override
    protected void verifyNeverShowDetails() { }

    @Override
    protected void verifyShowDetails() {}

    @Override
    protected void loadDetails() {
        List<Genre> genres = new ArrayList<Genre>();
        genres.add(Mockito.mock(Genre.class));
        TvDetail tvDetail = Mockito.mock(TvDetail.class);
        when(tvDetail.genres).thenReturn(genres);
        verify(tvDataSource).getDetails(any(String.class), any(Context.class), datasourceInterface.capture());
        datasourceInterface.getValue().onSuccess(tvDetail);
    }

    @Override
    protected void loadDetailsEmpty() {
        TvDetail tvDetail = Mockito.mock(TvDetail.class);
        when(tvDetail.genres).thenReturn(new ArrayList<Genre>());
        verify(tvDataSource).getDetails(any(String.class), any(Context.class), datasourceInterface.capture());
        datasourceInterface.getValue().onSuccess(tvDetail);
    }

    @Test
    public void whenRecommendationItemClickedGoToTvDetail(){
        Media media = Mockito.mock(Media.class);
        View[] views = new View[1];
        tvDetailPresenter.onRecommendationClicked(media, views);
        verify(tvView).goToTvDetailActivity(media, views);
    }

}


