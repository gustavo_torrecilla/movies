package com.movietime.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.movietime.media_model.Tv
import io.reactivex.Single

@Dao
interface TvDao {
    @Query("SELECT * FROM tv WHERE id IN (:id) LIMIT 1")
    fun getTv(id: String): Single<Tv>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTvShows(movies: List<Tv>)
}