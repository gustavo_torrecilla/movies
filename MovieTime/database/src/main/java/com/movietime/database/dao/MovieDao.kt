package com.movietime.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.movietime.media_model.Movie
import io.reactivex.Single

@Dao
interface MovieDao {
    @Query("SELECT * FROM movie WHERE id IN (:id) LIMIT 1")
    fun getMovie(id: String): Single<Movie>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMovies(movies: List<Movie>)
}