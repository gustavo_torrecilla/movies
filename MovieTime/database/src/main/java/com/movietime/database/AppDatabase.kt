package com.movietime.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.movietime.database.dao.MovieDao
import com.movietime.database.dao.TvDao
import com.movietime.media_model.Movie
import com.movietime.media_model.Tv

@Database(entities = arrayOf(Movie::class, Tv::class), version = 2)
abstract class AppDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDao
    abstract fun tvDao(): TvDao
}