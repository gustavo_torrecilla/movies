package com.movietime.network

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.movietime.media_model.Media
import com.movietime.media_model.Movie
import com.movietime.media_model.Tv
import com.movietime.media_model.Video
import com.movietime.app.client.deserializers.ListDeserializer
import com.movietime.app.client.deserializers.MediaListDeserializer
import com.movietime.app.client.interceptor.ApiKeyInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

private const val BASE_URL = "https://api.themoviedb.org/3/"

@InstallIn(SingletonComponent::class)
@Module
class NetworkModule {

    @Singleton
    @Provides
    fun provideRetrofit(
        httpClient: OkHttpClient,
        gson: Gson
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(httpClient)
            .build()
    }

    @Provides
    fun provideHttpClient(
        apiKeyInterceptor: ApiKeyInterceptor,
        httpLoggingInterceptor: HttpLoggingInterceptor
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(apiKeyInterceptor)
            .addInterceptor(httpLoggingInterceptor)
            .build()
    }

    @Provides
    fun provideLoginInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    @Provides
    fun provideApiKeyInterceptor(): ApiKeyInterceptor {
        return ApiKeyInterceptor()
    }

    @Provides
    fun providesGson(): Gson {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.registerTypeAdapter(
            object :
                TypeToken<@JvmSuppressWildcards List<Movie>>() {}.type,
            ListDeserializer<Movie>()
        )
        gsonBuilder.registerTypeAdapter(
            object :
                TypeToken<@JvmSuppressWildcards List<Tv>>() {}.type,
            ListDeserializer<Tv>()
        )
        gsonBuilder.registerTypeAdapter(
            object :
                TypeToken<@JvmSuppressWildcards List<Video>>() {}.type,
            ListDeserializer<Video>()
        )
        gsonBuilder.registerTypeAdapter(
            object :
                TypeToken<@JvmSuppressWildcards List<Media>>() {}.type,
            MediaListDeserializer()
        )
        return gsonBuilder.create()
    }
}