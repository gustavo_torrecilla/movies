# Movie Time
Esta aplicación muestra Series y Películas consumiendo la API de The Movie DB.

Para la comunicación con la API usé Retrofit + Okhttp + Gson. En el paquete client se encuentran todas las clases relacionadas con la capa de red.  
En el paquete datasource hay wrappers de las llamadas de retrofit. En general considero buena práctica hacer wrappers sobre librerías externas. Ya que reducen la dependencia que tengo con la misma. En este caso si quisiera cambiar las librerías que utilizo para la comunicación http tendría un impacto menor en mi código (solo debo cambiar las implementaciones dentro de este paquete). Además me permite testar con mayor facilidad.

Para representar las clases relacionadas a la información de películas y series, cree una jerarquía donde la clase padre es Media. Esto me permitió crear activities y fragments que sepan mostrar tanto información relacionada a Películas como a Series

### Alcance
La aplicación tiene:

- Tres categorías de películas y series.
- Detalle de cada película y series.
- Funciona tanto online como offline (cache).
- Videos en el detalle de cada ítem.
- Transiciones, Animaciones, UI/UX.
- Filtro offline por categorías.
- Buscador online. 
- Test unitarios de los presenters
- Test unitarios de la activity de detalle de pelicula (hice solo de una activity a modo de ejemplo).


### Mejoras a futuro
- Agregar paginación. (Para esto utilizaría PagedList con PageKeyedDataSource)
- Mostrar loadings y handlear errores de request.
- Acomodar los generos para que se vean bien cuando hay muchos. (ahora se cortan si no entran en la pantalla)

### Observaciones
- En la pantalla de Detalles, decidí mostrar la info una vez terminada la transición, si es que para ese momento logré obtener la info de la API. En caso de no tener la info, la muestro ni bien la obtengo. Esto me permitió que la animación de la transición sea mucho mas fluída.
- En la pantalla de busqueda se muestran sugerencias pasados los 500ms de que no se escribió. Los clicks en esas sugerencias no realizan la busqueda, sino que abren directamente el detalle de dicha sugerencia.
- La aplicación fue desarrollada siguiendo el patrón MVP.

### Preguntas

1- En qué consiste el principio de responsabilidad única? Cuál es su propósito?

Consiste en que un objeto solo tenga una responsabilidad clara, y no realice mas cosas de las que debería. Esto favorece la alta cohesión en el diseño.   
Entre sus propósitos se destaca:

- Si un objeto tiene una única responsabilidad, entonces su implementación cambia solo si cambia su responsabilidad. Es decir que hay un único eje de cambio.
- Nos permite tener un código más modular.
- Es más fácil de entender cual es la responsabilidad de un objeto (Si un objeto tiene varias responsabilidades es complicado entender en que punto terminan sus responsabilidades)  


2- Qué características tiene, según su opinión, un “buen” código o código limpio?

Según mi opinión un buen código debe ser modular, tener test, y deve ser facil de leer y entender.
Para lograr modularidad se debe buscar máxima cohesión y mínimo acoplamiento.  
Máxima cohesión está relacionada a la respuesta de la pregunta anterior.  
El acoplamiento se corresponde a la relación entre los distintos componentes del modelo. Cuantas más dependencias tiene un componente más acoplado está. Si tenemos un cambio en un componente, va a impactar en todos los que dependan del mismo, por lo tanto si tenemos alto acoplamiento los cambios impactan en más partes de nuestro modelo.  
Por otro lado, es importante entender que un desarrollador pasa más tiempo leyendo código que implementandolo, es por eso que el código debe ser facil de leer y entender. Además esto permite que sea facil de modificar. Esto es importante ya que el código suele estar en una evolución constante.  
Los test, por otro lado, aumentan la confianza que tenemos en nuestro código. Nos permiten realizar cambios con mayor seguridad, ya que luego de realizar un cambio podemos correr los test para ver que todo siga funcionando.  
Además un buen código en mi opinion debe seguir las siguientes heurísticas.  
Se deben favorecer los objetos inmutables, esto permite abstraernos del paso del tiempo, ya que en cualquier parte del código en el que accedamos al objeto su valor no cambiará. Esto NO significa que TODOS los objetos deben ser inmutables, sino que su esencia no debe cambiar. Hay casos donde tiene sentido que un objeto sea mutable.  
Los objetos deben ser completos. Permitir objetos incompletos te lleva a un estado de incertidumbre a la hora de usar el mismo.  
Los objetos deben ser validos (Fail Fast). No debería permitir la construcción de objetos inválidos.
